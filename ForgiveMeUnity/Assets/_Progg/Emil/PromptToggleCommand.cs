﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("Text Prompt", "Toggle Text Prompt", "Toggles text prompts on/off")]
public class PromptToggleCommand : Command
{
    public override void OnEnter()
    {
		PromptActivator.SetPromptActiveState(!PromptActivator.GetPromptActiveState());
		Continue();
	}
}
