﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("Lights", "Disable Lights", "Disables the lights that are selected in the LightDisabler")]
public class LightDisableCommand : Command
{
    public override void OnEnter()
    {
		LightDisabler lightDisabler = FindObjectOfType<LightDisabler>();
		lightDisabler.DisableLights();
		Continue();
	}
}
