﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//TODO: insert sound effect on successful pickup
public class PickupHandler : MonoBehaviour
{
    [Tooltip("The tag to check for to detect an inventory/pickup object")]
    [SerializeField] private string m_inventoryTag;
    [Tooltip("The UI prompt object to display when looking at a pickup object")]
    [SerializeField] private GameObject m_pickupPrompt;

	private bool m_useItemPlacementTag = true;
    private InventoryObject testObj;

    void Start()
    {
		InputHandler.Instance.PickupStateChange += TryPickupItem;
		InventoryHandler.ResetInventory();
	}

    // void Update()
    // {
    //     if (m_pickupPrompt != null)
    //     {
	// 		m_pickupPrompt.SetActive(false);
	// 		MouseController.m_MouseInstance.ActivateCrosshair(this);
	// 	}
        
	// 	RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
	// 	if (hit.transform != null && hit.transform.CompareTag(m_inventoryTag) && !m_pickupPrompt.activeInHierarchy)
	// 		if (m_pickupPrompt != null)
    //         {
	// 			m_pickupPrompt.SetActive(true);
	// 			MouseController.m_MouseInstance.DeactivateCrosshair(this);
	// 		}
    // }

    /// <summary>
    /// Method for detecting and picking up items
    /// </summary>
    void TryPickupItem()
    {
		RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
		if (hit.transform != null)
        {
            if (m_inventoryTag == string.Empty)
            {
                Debug.Log("No inventory/pickup tag assigned to the PickupHandler script on "
                    + name + "! Set a inventory/pickup tag in the inspector!");
            }
            if (hit.transform.CompareTag(m_inventoryTag))
            {
                if (hit.transform.name.Contains("Mirror"))
					AudioManager.Instance.ThunderBolt(Camera.main.transform.position);
				hit.transform.gameObject.SetActive(false);
                InventoryHandler.StoreInventoryItem(hit.transform.gameObject);
            }
        }

    }

	/// <summary>
	/// Method for attempting to place an item in world
	/// </summary>
	// void TryPlaceItem()
	// {
	// 	RaycastHit hit;
	// 	Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
	// 	//Cast ray in camera look direction with max distance & collect hit data
	// 	if (Physics.Raycast(ray, out hit, m_maxPickupDist))
	// 	{
	// 		//If using a tag for detecting places where items can be placed
	// 		if (m_useItemPlacementTag)
	// 		{
	// 			//Return out of the method if the hit has the wrong tag
	// 			if (!hit.transform.CompareTag(m_itemPlacementTag))
	// 				return;
	// 			//Try to place indexed item at the placement objects position
	// 			GameObject item = InventoryHandler.RemoveItemFromInventory(0);

	// 			//Helper method for verifying inventory contents
	// 			//InventoryHandler.GetInventoryStatus();

	// 			if (item == null)
	// 			{
	// 				print("No item to place in world!");
	// 				return;
	// 			}
	// 			item.SetActive(true);
	// 			item.transform.position = hit.transform.position;
	// 		}
	// 		//Not using tag detection, going with hit.normal + hit.point detection
	// 		else if (!m_useItemPlacementTag)
	// 		{
	// 			//Return out of the method if the hit is a storeable object
	// 			if (hit.transform.CompareTag(m_inventoryTag))
	// 				return;
	// 			//Try to place indexed item at hit-point + hit-normal
	// 			GameObject item = InventoryHandler.RemoveItemFromInventory(0);

	// 			//Helper method for verifying inventory contents
	// 			//InventoryHandler.GetInventoryStatus();

	// 			if (item == null)
	// 			{
	// 				print("No item to place in world!");
	// 				return;
	// 			}
	// 			item.SetActive(true);
	// 			item.transform.position = hit.point + hit.normal;
	// 		}
	// 	}
	// }

    private void OnEnable()
    {
        if (InputHandler.Instance != null)
        {
			InputHandler.Instance.PickupStateChange += TryPickupItem;
		}
    }

    private void OnDisable()
    {
        if (InputHandler.Instance != null)
        {
			InputHandler.Instance.PickupStateChange -= TryPickupItem;
		}
    }


}