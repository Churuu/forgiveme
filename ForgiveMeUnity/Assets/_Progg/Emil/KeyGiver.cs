﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;


public class KeyGiver : MonoBehaviour
{
	[SerializeField] private Flowchart m_flowchart;
	[SerializeField] private bool m_isCorrectKey;
	[SerializeField] private GameObject m_keyPrompt;

	private bool m_keyGot;

	void Start()
    {
        if (InputHandler.Instance != null)
        {
			InputHandler.Instance.PickupStateChange += AddKeyToInventory;
		}
    }

    void OnEnable()
    {
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange += AddKeyToInventory;
		}
    }

    void OnDisable()
    {
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange -= AddKeyToInventory;
		}
    }

	// void Update()
	// {
	// 	// if (m_gazePrompt == null)
	// 	// 	return;
	// 	// RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
	// 	// if (hit.transform == transform && !CompareTag("Examine") && !m_gazePrompt.activeInHierarchy)
	// 	// {
	// 	// 	m_gazePrompt.SetActive(true);
	// 	// }
	// 	// else if (hit.transform != transform || hit.transform == null)
	// 	// {
	// 	// 	m_gazePrompt.SetActive(false);
	// 	// }

	// 	// if (InventoryHandler.CompareName(name))
	// 	// 	return;
	// 	RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
	// 	if (hit.transform != null)
	// 	{
	// 		if (hit.transform == transform && !CompareTag("Examine"))
	// 		{
	// 			if (m_keyPrompt != null)
	// 				m_keyPrompt.SetActive(true);
	// 		}
	// 		else
	// 		{
	// 			if (m_keyPrompt != null)
	// 				m_keyPrompt.SetActive(false);
	// 		}
	// 	}
	// 	else if (m_keyPrompt != null && m_keyPrompt.activeInHierarchy)
	// 	{
	// 		m_keyPrompt.SetActive(false);
	// 	}
	// }

	void AddKeyToInventory()
    {
		if (m_keyGot)
			return;
		RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
        if (hit.transform == transform && !CompareTag("Examine"))
        {
			if (m_isCorrectKey)
            {
				m_flowchart.SendFungusMessage("Correct");
				InventoryHandler.StoreInventoryItem(gameObject);
				m_keyPrompt.SetActive(false);
				m_keyGot = true;
				gameObject.SetActive(false);
				this.enabled = false;
			}
            else
            {
				m_flowchart.SendFungusMessage("InCorrect");
			}
        }
	}
}
