﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowCaster : MonoBehaviour
{
	[SerializeField] private Transform[] m_shadowProjectors;
	[SerializeField] private float m_detectionRange, m_activeTime;

	private bool m_shadowActive;
	private float m_activationTime;
	private Transform m_shadow;

    // Update is called once per frame
    void Update()
    {
        if (m_shadowProjectors.Length == 0)
			return;
		if (m_shadowActive)
        {
            if (Time.time > m_activationTime + m_activeTime)
            {
				m_shadow?.gameObject.SetActive(false);
			}
        }
    }

    public void ActivateShadow()
    {
        if (m_shadowProjectors.Length == 0)
			return;
		m_shadow = GetCLosestShadow();
		m_shadowActive = m_shadow != null;
		m_activationTime = m_shadowActive ? Time.time : 0f;
		m_shadow?.gameObject.SetActive(true);
	}

    Transform GetCLosestShadow()
    {
		int closestIndex = -1;
		float closest = Mathf.Infinity;
		for (int i = 0; i < m_shadowProjectors.Length; i++)
        {
            if (Vector3.Distance(transform.position, m_shadowProjectors[i].position) < closest)
            {
				closest = Vector3.Distance(transform.position, m_shadowProjectors[i].position);
				closestIndex = i;
			}
        }
		return m_shadowProjectors[closestIndex];
	}
}
