﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

public class VaseObject : MonoBehaviour
{
    [Tooltip("The note to get when interacting with the vase")]
	[SerializeField] private GameObject m_note;
	// [Tooltip("The maximum distance to check for the vase with raycasting")]
	// [SerializeField] private float m_maxRaycastDist;
	// [Tooltip("The UI investigate prompt object to display when investigating the vase")]
	// [SerializeField] private GameObject m_investigationPrompt;
	[Tooltip("The UI prompt object to display when looking at the vase, before investigating it")]
	[SerializeField] private GameObject m_vasePrompt;
	// [Tooltip("The UI prompt object to display that the note is picked up, if desired")]
	// [SerializeField] private GameObject m_vaseNotePrompt;
	[SerializeField] private Flowchart m_vaseFlowchart;

	// [Tooltip("The radius of the area the raycast checks")]
	// [SerializeField] private float m_raycastRadius = 0.2f;

	//private bool m_disableGaze = false, m_investigating = false;

	// Start is called before the first frame update
	void Start()
    {
        if (InputHandler.Instance != null)
        {
			InputHandler.Instance.PickupStateChange += RecieveNote;
		}
    }

    // // Update is called once per frame
    // void Update()
    // {
	// 	// if (m_disableGaze)
	// 	// 	return;
	// 	// RaycastHit hit;
	// 	// Ray cameraRay = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
	// 	// if (Physics.SphereCast(cameraRay, m_raycastRadius, out hit, m_maxRaycastDist))
	// 	// {
	// 	// 	if (hit.transform == transform && !hit.transform.CompareTag("Examine"))
	// 	// 	{
	// 	// 		if (m_gazingPrompt != null)
	// 	// 			m_gazingPrompt.SetActive(true);
	// 	// 	}
	// 	// 	else
	// 	// 	{
	// 	// 		if (m_gazingPrompt != null)
	// 	// 			m_gazingPrompt.SetActive(false);
	// 	// 	}
	// 	// }
	// 	// else
	// 	// {
	// 	// 	if (m_gazingPrompt != null && m_gazingPrompt.activeInHierarchy)
	// 	// 		m_gazingPrompt.SetActive(false);
	// 	// }

	// 	// if (InventoryHandler.CompareName(m_note.name))
	// 	// 	return;
	// 	RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
	// 	if (hit.transform != null)
	// 	{
	// 		if (hit.transform == transform && !CompareTag("Examine"))
	// 		{
	// 			if (m_vasePrompt != null)
	// 				m_vasePrompt.SetActive(true);
	// 		}
	// 		else
	// 		{
	// 			if (m_vasePrompt != null)
	// 				m_vasePrompt.SetActive(false);
	// 		}
	// 	}
	// 	else if (m_vasePrompt != null && m_vasePrompt.activeInHierarchy)
	// 	{
	// 		m_vasePrompt.SetActive(false);
	// 	}
    // }

    public void RecieveNote()
    {
		//Play sound?
		if (InventoryHandler.CompareName(m_note.name))
			return;
		RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
		if (hit.transform == transform && !CompareTag("Examine"))
		{
			m_vaseFlowchart.SendFungusMessage("Say");
			InventoryHandler.StoreInventoryItem(m_note);
			m_note.SetActive(false);
			m_vasePrompt.SetActive(false);
			this.enabled = false;
			gameObject.tag = "Untagged";
			//if (ItemProgressionHandler.Instance != null)
			//ItemProgressionHandler.Instance.AdvancePhase();
		}
        // if (m_investigationPrompt != null)
		//     m_investigationPrompt.SetActive(false);
		// if (m_vaseNotePrompt != null)
		// {
		// 	StartCoroutine(DisplayPickupPrompt());
		// }
		// else
		// {
		// 	this.enabled = false;
		// }
	}

    // IEnumerator DisplayPickupPrompt()
    // {
	// 	m_vaseNotePrompt.SetActive(true);
	// 	yield return new WaitForSeconds(1.5f);
	// 	m_vaseNotePrompt.SetActive(false);
	// 	this.enabled = false;
	// }

	// private void InvestigateObject()
	// {
    //     if (m_investigating)
	// 		return;
	// 	RaycastHit hit;
	// 	Ray cameraRay = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
	// 	if (Physics.SphereCast(cameraRay, m_raycastRadius, out hit, m_maxRaycastDist))
    //     {
	// 		if (hit.transform == transform && !hit.transform.CompareTag("Examine"))
	// 		{
	// 			m_disableGaze = true;
	// 			if (m_gazingPrompt != null)
	// 				m_gazingPrompt.SetActive(false);
	// 			if (m_investigationPrompt != null)
	// 				m_investigationPrompt.SetActive(true);
	// 			MouseController.m_MouseInstance.LockUnlockCursor();
	// 			m_investigating = true;
	// 		}
    //     }
	// }

    void OnEnable()
    {
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange += RecieveNote;
		}
    }

    void OnDisable()
    {
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange -= RecieveNote;
		}
    }
}
