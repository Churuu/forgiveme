﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD;
using FMODUnity;

public class DoorChaosSequencer : MonoBehaviour
{

    [Tooltip("Objects to be switched out when closing door")]
	[SerializeField] private GameObject[] m_objectsToDeactivate;
    [Tooltip("Objects to be switched in when closing door")]
    [SerializeField] private GameObject[] m_objectsToActivate;
    [Tooltip("The moveable door object, needs to have a MoveInteractObject script on it!")]
	[SerializeField] private MoveInteractObject m_door;
    [Tooltip("Set if door should open automatically or not after chaos")]
	[SerializeField] private bool m_autoOpenDoor;
    [Tooltip("The key prefab object to pick up, it's used as a progression confirmation")]
	[SerializeField] private GameObject m_keyObject;
    [Tooltip("Audio duration + time to wait until the door is openable again")]
	[SerializeField] private float m_doorOpeningDelay;
	[SerializeField] [EventRef] private string m_sequenceAudio;
	private bool m_chaosEnabled = false, m_startedChaos = false;

	// Start is called before the first frame update
	void Start()
    {
        if (InputHandler.Instance != null)
        {
			InputHandler.Instance.PickupStateChange += ActivateChaos;
		}

		//Alt: change door tag after door is closed to prevent player from opening it prematurely
		if (m_autoOpenDoor && m_door != null)
			m_door.tag = "Untagged";
    }

    void OnEnable()
    {
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange += ActivateChaos;
		}
    }

    void OnDisable()
    {
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange -= ActivateChaos;
		}
    }

    void OnTriggerEnter()
    {
        if (m_chaosEnabled && !m_startedChaos)
        {
			m_startedChaos = true;
			StartCoroutine(DoChaos());
		}
    }

    IEnumerator DoChaos()
    {
		//Play sound
		if (!string.IsNullOrEmpty(m_sequenceAudio))
		{
			RuntimeManager.PlayOneShot(m_sequenceAudio, transform.position);
		}

        //Close door
        if (m_door != null)
		{
			m_door.Move();
			yield return new WaitUntil(() => m_door.GetCanMove());
		}

		//Change active objects
		foreach (var item in m_objectsToDeactivate)
        {
			if (item != null)
				item.SetActive(false);
		}
        foreach (var item in m_objectsToActivate)
        {
			if (item != null)
				item.SetActive(true);
		}

		//Alt: Simply call AdvancePhase from ItemProgressionHandler
        // if (ItemProgressionHandler.Instance != null)
		//     ItemProgressionHandler.Instance.AdvancePhase();

		//Play sound?

		//Wait for sound to be done
		yield return new WaitForSeconds(m_doorOpeningDelay);

		//Open door
        if (m_autoOpenDoor && m_door != null)
	    	m_door.Move();

		//Alt: change door tag back to move to make player able to open door again
		if (!m_autoOpenDoor && m_door != null)
            m_door.tag = "Move";
	}

    void ActivateChaos()
    {
        if (!m_chaosEnabled && m_keyObject != null && InventoryHandler.CompareName(m_keyObject.name))
		    m_chaosEnabled = true;
	}
}
