﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;

public class MoveTable : MonoBehaviour
{
    [EventRef]
    public string m_MovingTable;
    
    public void MoveTableFunction()
    {
        RuntimeManager.PlayOneShot(m_MovingTable, transform.position);
    }
    
}
