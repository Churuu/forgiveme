﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;

public class ItemProgressionHandler : MonoBehaviour
{

	public delegate void PhaseActivate(Phase phase);
	public event PhaseActivate m_phaseActivation;

	[SerializeField] private PhaseObjects[] m_phaseObjects = new PhaseObjects[]{};
	[SerializeField] private bool m_debugModeActive;

	public static ItemProgressionHandler Instance
    {
        get
        {
            return instance;
        }
    }

    private static ItemProgressionHandler instance;

	private Phase m_currentPhaseStep;

	private Dictionary<string, string> m_tagList = new Dictionary<string, string>();
	private const string UNTAGGED = "Untagged";
	private const string EXAMINE = "Examine";

	private static PhaseObjects[] m_staticPhaseObjects
	{
		get
		{
			return Instance.m_phaseObjects;
		}
	}
	private static Phase m_staticCurrentPhase
	{
		get
		{
			return Instance.m_currentPhaseStep;
		}
		set
		{
			Instance.m_currentPhaseStep = value;
		}
	}
	private static Dictionary<string, string> m_staticTagList
	{
		get
		{
			return Instance.m_tagList;
		}
	}
	private static PhaseActivate m_staticPhaseActivation
	{
		get
		{
			return Instance.m_phaseActivation;
		}
	}

	void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
		EnsureDisabledStates();
    }

	// void Update()
	// {
	// 	if (m_debugModeActive && Input.GetKeyDown(KeyCode.P))
	// 		AdvancePhase();
	// }

    void EnsureDisabledStates()
    {
        for (int i = 0; i < m_phaseObjects.Length; i++)
        {
			// if (m_phaseObjects[i].GetPhase() == PhaseStep.second ||
			//     m_phaseObjects[i].GetPhase() == PhaseStep.third)
			// {
			// 	if (m_phaseObjects[i].GetGameObjects().Length > 0)
			// 		foreach (var item in m_phaseObjects[i].GetGameObjects())
			// 		{
			// 			//item.SetActive(false);
			// 			//Save tag until re-activation
			// 			m_tagList.Add(item.name, item.tag);
			// 			//Change tag to "deactivate" object
			// 			item.tag = m_setAsExamineTag ? EXAMINE : UNTAGGED;
			// 		}
			// 	if (m_phaseObjects[i].GetMonoBehaviours().Length > 0)
			// 		foreach (var item in m_phaseObjects[i].GetMonoBehaviours())
			// 		{
			// 			item.enabled = false;
			// 		}
			// }
			if (m_phaseObjects[i].GetPhase() == Phase.first)
				continue;
			if (m_phaseObjects[i].GetGameObjects().Length > 0)
				foreach (var item in m_phaseObjects[i].GetGameObjects())
				{
					//item.SetActive(false);
					//Save tag until re-activation
					m_tagList.Add(item.name, item.tag);
					//Change tag to "deactivate" object
					item.tag = m_phaseObjects[i].m_SetItemAsExamine ? EXAMINE : UNTAGGED;
				}
			if (m_phaseObjects[i].GetMonoBehaviours().Length > 0)
				foreach (var item in m_phaseObjects[i].GetMonoBehaviours())
				{
					item.enabled = false;
				}
        }
    }

    /// <summary>
    /// Call method to de/activate objects/scripts for next phase
    /// </summary>
    public void AdvancePhase()
    {
		m_currentPhaseStep = (Phase)(((int)m_currentPhaseStep) + 1);
		ActivateObjects();
    }

	#if UNITY_EDITOR
	[MenuItem("Item Progression/Advance Phase")]
	static void MenuAdvancePhase()
	{
		m_staticCurrentPhase = (Phase)(((int)m_staticCurrentPhase) + 1);
		StaticActivateObjects();
	}
	#endif

    /// <summary>
    /// Call to load into correct progression phase-step
    /// </summary>
    /// <param name="phase"></param>
    public void LoadToPhase(Phase phase)
    {
		EnsureDisabledStates();
        for (int i = 0; i < (int)phase; i++)
        {
			m_currentPhaseStep = (Phase)i;
			ActivateObjects();
		}
	}

    /// <summary>
    /// Call to get currently active phase
    /// </summary>
    /// <returns></returns>
    public Phase GetCurrentPhase()
    {
		return m_currentPhaseStep;
	}

    void ActivateObjects()
    {
		PhaseObjects phaseObj = m_phaseObjects.Where(x => x.GetPhase() == m_currentPhaseStep).FirstOrDefault();
		if (phaseObj != null)
		{
			if (phaseObj.GetGameObjects().Length > 0)
			{
				foreach (var item in phaseObj.GetGameObjects())
				{
					//item.SetActive(true);
					//Apply old tag for re-activation
					string tag;
					if (m_tagList.TryGetValue(item.name, out tag))
					{
						item.tag = tag;
					}
				}
			}
			if (phaseObj.GetMonoBehaviours().Length > 0)
			{
				foreach (var item in phaseObj.GetMonoBehaviours())
				{
					item.enabled = false;
				}
			}
			if (m_phaseActivation != null)
				m_phaseActivation(phaseObj.GetPhase());
		}
    }

	static void StaticActivateObjects()
	{
		PhaseObjects phaseObj = m_staticPhaseObjects.Where(x => x.GetPhase() == m_staticCurrentPhase).FirstOrDefault();
		if (phaseObj != null)
		{
			if (phaseObj.GetGameObjects().Length > 0)
			{
				foreach (var item in phaseObj.GetGameObjects())
				{
					//item.SetActive(true);
					//Apply old tag for re-activation
					string tag;
					if (m_staticTagList.TryGetValue(item.name, out tag))
					{
						item.tag = tag;
					}
				}
			}
			if (phaseObj.GetMonoBehaviours().Length > 0)
			{
				foreach (var item in phaseObj.GetMonoBehaviours())
				{
					item.enabled = false;
				}
			}
			if (m_staticPhaseActivation != null)
				m_staticPhaseActivation(phaseObj.GetPhase());
		}
	}

	#if UNITY_EDITOR
	[MenuItem("Item Progression/Show Current Phase")]
	static void ShowCurrentPhaseInLog()
	{
		Debug.Log("Current phase: " + m_staticCurrentPhase.ToString());
	}
	#endif

    public enum Phase
    {
        first, second, third, fourth, fifth, sixth, seventh, eight, ninth, tenth
    }

    [Serializable]
    public class PhaseObjects
    {
        [SerializeField] private Phase m_phaseStep;
        [Tooltip("Gameobjects that should be toggled in/active for a specific phase")]
        [SerializeField] private GameObject[] m_phaseGameObjects;
        [Tooltip("Scripts that should be toggled in/active for a specific phase")]
        [SerializeField] private MonoBehaviour[] m_phaseObjectScripts;

		public bool m_SetItemAsExamine;

        public GameObject[] GetGameObjects()
        {
            return m_phaseGameObjects;
        }

        public MonoBehaviour[] GetMonoBehaviours()
        {
            return m_phaseObjectScripts;
        }

        public Phase GetPhase()
        {
			return m_phaseStep;
		}
    }
}