﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightController : MonoBehaviour
{
	[SerializeField] private bool m_activateOnPhaseChange;
	[SerializeField] private Material m_highlightMaterial;
	[SerializeField] private ItemProgressionHandler.Phase m_activationPhase;
	private Renderer m_renderer;
	private Material m_regularMaterial;

	void Start()
    {
		m_renderer = GetComponent<Renderer>();
		m_regularMaterial = m_renderer.material;
	}

	void OnEnable()
    {
        if (m_activateOnPhaseChange)
        {
			ItemProgressionHandler.Instance.m_phaseActivation += SwitchToHighlight;
		}
    }

    void OnDisable()
    {
		if (m_activateOnPhaseChange)
		{
			ItemProgressionHandler.Instance.m_phaseActivation -= SwitchToHighlight;
		}
    }

    void SwitchToHighlight(ItemProgressionHandler.Phase phase)
    {
        if (phase != m_activationPhase)
			return;
		m_renderer.material = m_highlightMaterial;
    }

    public void SwitchToHighlight()
    {
        if (m_activateOnPhaseChange)
			return;
		m_renderer.material = m_highlightMaterial;
	}

    public void SwitchToRegular()
    {
		m_renderer.material = m_regularMaterial;
	}
}
