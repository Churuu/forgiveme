﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveObjectHandler : MonoBehaviour
{
	[Tooltip("The tag to check for to detect a move object")]
	[SerializeField] private string m_moveObjectTag;
	[Tooltip("The UI prompt object to display when looking at a move object")]
	[SerializeField] private GameObject m_moveObjectPrompt;

	private MoveInteractObject testObj;

	// Start is called before the first frame update
	void Start()
	{
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange += TryMoveObject;
		}
	}

	// void Update()
	// {
	// 	if (m_moveObjectPrompt != null)
	// 	{
	// 		m_moveObjectPrompt.SetActive(false);
	// 		MouseController.m_MouseInstance.ActivateCrosshair(this);
	// 	}

	// 	RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
	// 	if (hit.transform != null && hit.transform.CompareTag(m_moveObjectTag)
	// 		&& hit.transform.GetComponent<MoveInteractObject>().GetCanMove())
	// 		if (m_moveObjectPrompt != null && !m_moveObjectPrompt.activeInHierarchy)
	// 		{
	// 			m_moveObjectPrompt.SetActive(true);
	// 			MouseController.m_MouseInstance.DeactivateCrosshair(this);
	// 		}
	// }

	/// <summary>
	/// Method for detecting and activating move objects
	/// </summary>
	void TryMoveObject()
	{
		RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
		if (hit.transform != null)
		{
			if (m_moveObjectTag == string.Empty)
			{
				Debug.Log("No move tag assigned to the MoveObjectHandler script on "
					+ name + "! Set a move tag in the inspector!");
			}
			//Check if hit registered a move object
			if (hit.transform.CompareTag(m_moveObjectTag))
			{
				//Activate move function
				MoveInteractObject moveObj = hit.transform.GetComponent<MoveInteractObject>();
				moveObj.Move();
			}
		}
	}

	private void OnEnable()
	{
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange += TryMoveObject;
		}
	}

	private void OnDisable()
	{
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange -= TryMoveObject;
		}
	}
}