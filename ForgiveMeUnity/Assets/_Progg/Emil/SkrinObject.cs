﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using FMODUnity;
using FMOD;

public class SkrinObject : MonoBehaviour
{
    [Tooltip("The artefact to give the player")]
	[SerializeField] private GameObject m_artefact;
    [Tooltip("The key pickup object")]
	[SerializeField] private GameObject m_keyObject;
	[SerializeField] private Flowchart m_flowchart;
	[SerializeField] private GameObject m_skrinPrompt;
	[SerializeField] private GameObject m_doorknockObject;

	[SerializeField] private StudioEventEmitter m_stingerGeneral;

	void Start()
    {
        if (InputHandler.Instance != null)
        {
			InputHandler.Instance.PickupStateChange += TryGetArtefact;
		}
    }

    void OnEnable()
    {
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange += TryGetArtefact;
		}
    }

    void OnDisable()
    {
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange -= TryGetArtefact;
		}
    }

    // void Update()
    // {
	// 	// if (m_gazePrompt == null)
	// 	// 	return;
	// 	// RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
	// 	// if (hit.transform == transform && !CompareTag("Examine") && !m_gazePrompt.activeInHierarchy)
	// 	// {
	// 	// 	m_gazePrompt.SetActive(true);
	// 	// }
	// 	// else if (hit.transform != transform || hit.transform == null)
	// 	// {
	// 	// 	m_gazePrompt.SetActive(false);
	// 	// }

	// 	// if (InventoryHandler.CompareName(m_artefact.name))
	// 	// 	return;
	// 	RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
	// 	if (hit.transform != null)
	// 	{
	// 		if (hit.transform == transform && !CompareTag("Examine"))
	// 		{
	// 			if (m_skrinPrompt != null)
	// 				m_skrinPrompt.SetActive(true);
	// 		}
	// 		else
	// 		{
	// 			if (m_skrinPrompt != null)
	// 				m_skrinPrompt.SetActive(false);
	// 		}
	// 	}
	// 	else if (m_skrinPrompt != null && m_skrinPrompt.activeInHierarchy)
	// 	{
	// 		m_skrinPrompt.SetActive(false);
	// 	}
    // }

    void TryGetArtefact()
    {
		if (InventoryHandler.CompareName(m_artefact.name))
			return;
		RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
        if (hit.transform == transform && !CompareTag("Examine"))
        {
			if (InventoryHandler.CompareName(m_keyObject.name))
            {
				m_flowchart.SendFungusMessage("Key");
				InventoryHandler.StoreInventoryItem(m_artefact);
				m_artefact.SetActive(false);
				if (ItemProgressionHandler.Instance != null)
					ItemProgressionHandler.Instance.AdvancePhase();
				m_skrinPrompt.SetActive(false);
				AudioManager.Instance.ThunderBolt(Camera.main.transform.position);
                AudioManager.Instance.Case(Camera.main.transform.position);
                if (m_doorknockObject != null)
					m_doorknockObject.SetActive(true);
				this.enabled = false;
				gameObject.tag = "Untagged";
				//m_stingerGeneral.Play(); Missing stinger???
			}
            else
            {
				m_flowchart.SendFungusMessage("No_Key");
			}
		}
	}
}
