﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using FMOD;
using FMODUnity;

public class Teabag : MonoBehaviour
{
    [Tooltip("The artefact to give the player")]
	[SerializeField] private GameObject m_artefact;
	[SerializeField] private Flowchart m_flowchart;
	[SerializeField] private bool m_correctTea;
	[SerializeField] private GameObject m_teaPrompt;

	[SerializeField] private StudioEventEmitter m_stingerGeneral;

	// Start is called before the first frame update
	void Start()
    {
        if (InputHandler.Instance != null)
        {
			InputHandler.Instance.PickupStateChange += TryGiveArtefact;
		}
    }

    void OnEnable()
    {
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange += TryGiveArtefact;
		}
    }

    void OnDisable()
    {
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange -= TryGiveArtefact;
		}
    }

	// void Update()
	// {
	// 	// if (m_gazePrompt == null)
	// 	// 	return;
	// 	// RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
	// 	// if (hit.transform == transform && !CompareTag("Examine") && !m_gazePrompt.activeInHierarchy)
	// 	// {
	// 	// 	m_gazePrompt.SetActive(true);
	// 	// }
	// 	// else if (hit.transform != transform || hit.transform == null)
	// 	// {
	// 	// 	m_gazePrompt.SetActive(false);
	// 	// }
	// 	// if (InventoryHandler.CompareName(m_artefact.name))
	// 	// 	return;
	// 	RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
	// 	if (hit.transform != null)
	// 	{
	// 		if (hit.transform == transform && !CompareTag("Examine"))
	// 		{
	// 			if (m_teaPrompt != null)
	// 				m_teaPrompt.SetActive(true);
	// 		}
	// 		else
	// 		{
	// 			if (m_teaPrompt != null)
	// 				m_teaPrompt.SetActive(false);
	// 		}
	// 	}
	// 	else if (m_teaPrompt != null && m_teaPrompt.activeInHierarchy)
	// 	{
	// 		m_teaPrompt.SetActive(false);
	// 	}
	// }

    void TryGiveArtefact()
    {
		if (InventoryHandler.CompareName(m_artefact.name))
			return;
		RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
		if (hit.transform == transform && !CompareTag("Examine"))
        {
			if (m_correctTea)
            {
				m_flowchart.SendFungusMessage("Found");
				InventoryHandler.StoreInventoryItem(m_artefact);
				m_artefact.SetActive(false);
				m_teaPrompt.SetActive(false);
				AudioManager.Instance.ThunderBolt(Camera.main.transform.position);
				this.enabled = false;
				FindObjectOfType<Ritual>().StartRadio();
				//m_stingerGeneral.Play(); missing stinger????
				gameObject.tag = "Untagged";
			}
            else
            {
				m_flowchart.SendFungusMessage("Wrong");
			}
		}
	}
}
