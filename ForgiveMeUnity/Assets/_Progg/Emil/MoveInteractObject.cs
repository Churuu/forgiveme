﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD;
using FMODUnity;
using System;

//TODO: insert sound effect on move action
public class MoveInteractObject : MonoBehaviour
{
	[Header("Displayed with a red cube in world")]
	[Tooltip("The object to move to when activated")]
	[SerializeField] private Transform m_moveToObject;
	[Tooltip("Higher value = slower movement")]
	[SerializeField] private float m_moveSpeedToTarget, m_moveSpeedBack;
	[Tooltip("Higher value = slower movement")]
	[SerializeField] private float m_rotateSpeedToTarget, m_rotateSpeedBack;
	[Tooltip("Sets how the movement will behave when moving")]
	[SerializeField] private LeanTweenType m_moveEasingType;
	[Tooltip("Sets whether the object can move/rotate to it's start position/rotation or not")]
	[SerializeField] private MovementBehaviour m_moveBehaviour = MovementBehaviour.simple;
	[Tooltip("Sets if the object moves, rotates, or both")]
	[SerializeField] private TranslationBehaviour m_translationBehaviour;
	[SerializeField] private HighlightController m_highlightController;
	[SerializeField] [EventRef] private string m_audioStartMove, m_audioEndMove;
	[SerializeField] private bool m_moveOnAwake;

	private bool m_atTarget, m_startedMove, m_startedRotate;
	private Vector3 m_startPos, m_startRot, m_targetPos;
	private Quaternion m_lookRotation;

	void Start()
	{
		//Save eventual starting/reset values
		m_startPos = transform.position;
		m_startRot = transform.eulerAngles;
		//Create look rotation for later use
		if (m_moveToObject != null)
		{
			//Since moveToObject is a child, transform the position into world space & save that value
			m_targetPos = transform.TransformPoint(m_moveToObject.localPosition);
			m_lookRotation =
				Quaternion.LookRotation(((m_moveToObject.position + m_moveToObject.forward) - m_moveToObject.position).normalized,
					((m_moveToObject.position + m_moveToObject.up) - m_moveToObject.position).normalized);

			if (m_moveOnAwake)
			{
				Move();
			}
		}
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		if (m_moveToObject != null)
			Gizmos.DrawCube(transform.TransformPoint(m_moveToObject.localPosition), Vector3.one * 0.25f);
	}

	public void Move()
	{
		if (!GetCanMove())
			return;
		if (m_moveToObject != null)
		{
			if (m_highlightController != null)
				m_highlightController.SwitchToRegular();
			if (!string.IsNullOrEmpty(m_audioStartMove))
			{
				RuntimeManager.PlayOneShot(m_audioStartMove, transform.position);
			}

			switch (m_translationBehaviour)
			{
				case TranslationBehaviour.moveOnly:
					//Do movement
					MoveBehaviour();
					return;
				case TranslationBehaviour.rotateOnly:
					//Do rotation
					Rotate();
					return;
				case TranslationBehaviour.rotateAndMove:
					//Do rotation
					Rotate();
					//Do movement
					MoveBehaviour();
					return;
			}
		}
		//Play sound?
	}

	public bool GetCanMove()
	{
		return m_moveBehaviour == MovementBehaviour.backForth && !(m_startedMove || m_startedRotate)
			|| m_moveBehaviour == MovementBehaviour.simple && !(m_startedMove || m_startedRotate);
	}

	void Rotate()
	{
		//Check which rotation behaviour is selected
		switch (m_moveBehaviour)
		{
			//If rotation to target & back to start value is desired
			case MovementBehaviour.backForth:
				//Check if rotated to target -- if no
				StartCoroutine(RotateTo(m_atTarget));
				return;
				//If simple rotation to target once is desired
			case MovementBehaviour.simple:
				//Do rotation
				LeanTween.rotate(gameObject, m_lookRotation.eulerAngles, m_rotateSpeedToTarget)
					.setEase(m_moveEasingType)
					.setOnComplete(() => OnStoppedMoving());
				m_startedRotate = true;
				return;
		}
	}

	void MoveBehaviour()
	{
		//Check which movement behaviour is selected
		switch (m_moveBehaviour)
		{
			//If movement to target & back to start value is desired
			case MovementBehaviour.backForth:
				//If not at target position, move to target position
				StartCoroutine(MoveTo(m_atTarget));
				return;
				//If simple movement to target once is desired, move to target position
			case MovementBehaviour.simple:
				LeanTween.move(gameObject, m_targetPos, m_moveSpeedToTarget)
					//.setMoveToTransform()
					.setEase(m_moveEasingType)
					.setOnComplete(() => OnStoppedMoving());
				m_startedMove = true;
				return;
		}
	}

	IEnumerator RotateTo(bool target)
	{
		m_startedRotate = true;
		if (!target)
		{
			//Do rotation
			LeanTween.rotate(gameObject, m_lookRotation.eulerAngles, m_rotateSpeedToTarget)
				.setEase(m_moveEasingType)
				.setOnComplete(() => m_startedRotate = false);
			yield return new WaitUntil(() => !m_startedRotate);
			OnStoppedMoving();
			m_atTarget = true;
		}
		else //Check if rotated to target -- if yes, rotate back to start
		{
			//Do rotation
			LeanTween.rotate(gameObject, m_startRot, m_rotateSpeedBack)
				.setEase(m_moveEasingType)
				.setOnComplete(() => m_startedRotate = false);
			yield return new WaitUntil(() => !m_startedRotate);
			OnStoppedMoving();
			m_atTarget = false;
		}
	}

	IEnumerator MoveTo(bool target)
	{
		m_startedMove = true;
		if (!target)
		{
			LeanTween.move(gameObject, m_targetPos, m_moveSpeedToTarget)
				//.setMoveToTransform()
				.setEase(m_moveEasingType)
				.setOnComplete(() => m_startedMove = false);
			yield return new WaitUntil(() => !m_startedMove);
			OnStoppedMoving();
			m_atTarget = true;
		}
		else //If at target position, move to start position
		{
			LeanTween.move(gameObject, m_startPos, m_moveSpeedBack)
				.setEase(m_moveEasingType)
				.setOnComplete(() => m_startedMove = false);
			yield return new WaitUntil(() => !m_startedMove);
			OnStoppedMoving();
			m_atTarget = false;
		}
	}

	void OnStoppedMoving()
	{
		if (!string.IsNullOrEmpty(m_audioEndMove))
		{
			RuntimeManager.PlayOneShot(m_audioEndMove, transform.position);
		}
	}

	private enum MovementBehaviour
	{
		backForth,
		simple
	}

	private enum TranslationBehaviour
	{
		rotateOnly, moveOnly, rotateAndMove
	}
}