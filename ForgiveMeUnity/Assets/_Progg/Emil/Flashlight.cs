﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: insert sound logic
public class Flashlight : MonoBehaviour
{
    [SerializeField] private Light m_light;
	private MeshRenderer m_model;

	void Start()
    {
        m_model = GetComponent<MeshRenderer>();
        m_model.enabled = false;
        if (m_light != null)
            m_light.enabled = false;
    }

    /// <summary>
    /// Toggles the flashlight
    /// </summary>
    public void ToggleFlashlight()
    {
		m_light.enabled = !m_light.enabled;
		m_model.enabled = !m_model.enabled;
	}

}