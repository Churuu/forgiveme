﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

//TODO: insert sound logic
public class LightDisabler : MonoBehaviour
{
    [Tooltip("The light components that should be disabled")]
    [SerializeField] private Light[] m_lightSources;
	[Tooltip("The lowest light value to set on the lights when disabling the lights, a value of 0 will disable them")]
	[SerializeField] private float m_lowestLightValue;
	[Tooltip("The phase to turn off the lights")]
	[SerializeField] private ItemProgressionHandler.Phase m_activationPhase;
	[SerializeField] private Transform[] m_audioEmitters;
	[SerializeField] private bool m_useActivationPhase;

	void Start()
	{
		if (ItemProgressionHandler.Instance != null && m_useActivationPhase)
		{
			ItemProgressionHandler.Instance.m_phaseActivation += DisableLightsOnPhase;
		}
	}

	void OnEnable()
    {
		if (ItemProgressionHandler.Instance != null && m_useActivationPhase)
		{
			ItemProgressionHandler.Instance.m_phaseActivation += DisableLightsOnPhase;
		}
    }

    void OnDisable()
    {
		if (ItemProgressionHandler.Instance != null && m_useActivationPhase)
		{
			ItemProgressionHandler.Instance.m_phaseActivation -= DisableLightsOnPhase;
		}
    }

    /// <summary>
    /// Call method to disable lights
    /// </summary>
    public void DisableLights()
    {
		if (m_useActivationPhase)
			return;
		if (m_lightSources.Length <= 0)
			return;
		AudioManager.Instance.ThunderBolt(Camera.main.transform.position);
		for (int i = 0; i < m_lightSources.Length; i++)
		{
			if (m_lowestLightValue <= 0)
				m_lightSources[i].enabled = false;
			else
				m_lightSources[i].intensity = m_lowestLightValue;
		}
		FindObjectOfType<RadioManager>().StopRadio();
		if (!(m_audioEmitters.Length > 0))
			return;
		for (int i = 0; i < m_audioEmitters.Length; i++)
		{
			m_audioEmitters[i].GetComponent<StudioEventEmitter>().Stop();
			m_audioEmitters[i].GetComponent<StudioEventEmitter>().enabled = false;
		}
		//Play sound?
	}

	void DisableLightsOnPhase(ItemProgressionHandler.Phase phase)
	{
		if (phase != m_activationPhase)
			return;
		if (m_lightSources.Length <= 0)
			return;
		AudioManager.Instance.ThunderBolt(Camera.main.transform.position);
		for (int i = 0; i < m_lightSources.Length; i++)
		{
			if (m_lowestLightValue <= 0)
				m_lightSources[i].enabled = false;
			else
				m_lightSources[i].intensity = m_lowestLightValue;
		}
		FindObjectOfType<RadioManager>().StopRadio();
		if (!(m_audioEmitters.Length > 0))
			return;
		for (int i = 0; i < m_audioEmitters.Length; i++)
		{
			m_audioEmitters[i].GetComponent<StudioEventEmitter>().Stop();
			m_audioEmitters[i].GetComponent<StudioEventEmitter>().enabled = false;
		}
		//Play sound?
	}

	public bool GetAreLightsDisabled()
	{
		if (m_lightSources.Length > 0)
			return !m_lightSources[0].enabled;
		else
			return false;
	}
    //Alt: subscribe DisableLights method to an event
}