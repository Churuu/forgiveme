﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class DisplayActionToggleCommand : Command
{
    public override void OnEnter()
    {
		DisplayAction display = FindObjectOfType<DisplayAction>();
		display.SetDisplayActiveState(!display.GetDisplayActiveState());
		Continue();
	}
}
