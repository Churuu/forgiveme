﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class PromptActivator : MonoBehaviour
{
	[SerializeField] private TagText[] m_tagTexts;

	private static Text m_promptText;
	private static bool m_promptActive = true;

	void Awake()
    {
		m_promptText = GetComponent<Text>();
		m_promptText.text = "";
	}

    void Update()
    {
        if (!m_promptActive)
			return;
        if (m_tagTexts.Length == 0)
			return;
		m_promptText.text = "";
		MouseController.m_MouseInstance.ActivateCrosshair(this);

		RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
		if (hit.collider != null && !hit.transform.CompareTag("Untagged"))
		{
            for (int i = 0; i < m_tagTexts.Length; i++)
            {
                if (hit.transform.tag == m_tagTexts[i].m_objectTag)
                {
					m_promptText.text = m_tagTexts[i].m_promptText;
					MouseController.m_MouseInstance.DeactivateCrosshair(this);
					return;
				}
            }
		}
    }

    public static void SetPromptActiveState(bool state)
    {
		m_promptActive = state;
        if (state == false)
			m_promptText.text = string.Empty;
	}

	public static bool GetPromptActiveState()
	{
		return m_promptActive;
	}

    [Serializable]
	public struct TagText
	{
		public string m_promptText;
		public string m_objectTag;
	}
}
