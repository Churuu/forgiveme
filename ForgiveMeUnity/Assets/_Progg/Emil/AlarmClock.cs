﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;

//TODO: insert sound logic
public class AlarmClock : MonoBehaviour
{
    [SerializeField] private StudioEventEmitter m_AlarmEvent;

	[Tooltip("The UI prompt object to display when looking at the alarm clock")]
	[SerializeField] private GameObject m_alarmClockPrompt;
	[Tooltip("Time to wait until the alarm stops playing")]
	[SerializeField] private float m_alarmMaxAliveTime;
	[Tooltip("Time to wait until alarm starts playing")]
	[SerializeField] private float m_alarmStartDelay;
    [SerializeField] private GameObject m_Player;

    private bool m_alarmStarted;

	// Start is called before the first frame update
	void Start()
	{
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange += DeActivateAlarm;
		}
		if (m_alarmStartDelay <= 0.0f)
			m_AlarmEvent.Play();
		//StartAlarm();
    }

	// Update is called once per frame
	void Update()
	{
		// RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
		// if (hit.transform != null)
		// {
		// 	if (hit.transform == transform)
		// 	{
		// 		m_lookingAtAlarm = true;
		// 		if (m_alarmClockPrompt != null)
		// 			m_alarmClockPrompt.SetActive(true);
		// 	}
		// 	else
		// 	{
		// 		m_lookingAtAlarm = false;
		// 		if (m_alarmClockPrompt != null)
		// 			m_alarmClockPrompt.SetActive(false);
		// 	}
		// }
		// else
		// {
		// 	m_lookingAtAlarm = false;
		// 	if (m_alarmClockPrompt != null && m_alarmClockPrompt.activeInHierarchy)
		// 		m_alarmClockPrompt.SetActive(false);
		// }

		if (m_alarmStartDelay > 0.0f && !m_alarmStarted)
		{
			if (Time.time >= m_alarmStartDelay)
			{
				m_alarmStarted = true;
				m_AlarmEvent.Play();
                transform.tag = "Alarm";
            }
		}

		if (Time.time >= m_alarmStartDelay + m_alarmMaxAliveTime)
		{
			m_AlarmEvent.Stop();
			m_alarmClockPrompt.SetActive(false);
            transform.tag = "Untagged";
            this.enabled = false;
		}
	}

	void DeActivateAlarm()
	{
		RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
		if (hit.transform == transform)
		{
			m_AlarmEvent.Stop();
            AudioManager.Instance.AlarmOff(m_Player.transform.position);
			m_alarmClockPrompt.SetActive(false);
            transform.tag = "Untagged";
			this.enabled = false;
		}
	}

	// void StartAlarm()
	// {
	// 	StartCoroutine(ActivateAlarm());
	// }

	// IEnumerator ActivateAlarm()
	// {
	// 	yield return new WaitForSeconds(m_alarmMaxAliveTime);
    //     m_AlarmEvent.Stop();
	// 	m_alarmClockPrompt.SetActive(false);
	// 	this.enabled = false;
	// }

	private void OnEnable()
	{
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange += DeActivateAlarm;
		}
	}

	private void OnDisable()
	{
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange -= DeActivateAlarm;
		}
	}
}