﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;
using FMOD;
using System;
using System.Runtime.CompilerServices;

[Serializable]
public class Songs
{
    [EventRef]
    public string m_SongPath;
    public EventInstance m_SongInstance;
}

public class MusicHandler : MonoBehaviour
{

    public StudioEventEmitter ritual;
    public StudioEventEmitter stingerlaura;
    float progress;


    [EventRef]
    [SerializeField] string SymbolPuzzle;
    [SerializeField] EventInstance SymbolMusic;
    // Start is called before the first frame update

    public static MusicHandler Instance;

    public RitualMusic RitualMusic;

    public Songs m_RitualBookSong;

    void Start()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);

        SymbolMusic = RuntimeManager.CreateInstance(SymbolPuzzle);

    }

    public void UpdateSymbolProgress(float intensity)
    {
        SymbolMusic.setParameterByName("Progress", intensity);
    }

    public void StartSymbolmusic()
    {
        SymbolMusic.start();
    }
    public void StopSymbolmusic()
    {
        SymbolMusic.release();
    }


    public void ritualmusic()
    {

        if (ritual != null)
        {
            ritual.SetParameter("RitualIntensity", 1.0f);
            ritual.Stop();
        }

    }

    public static void PlayMusic(Songs song)
    {
        song.m_SongInstance = RuntimeManager.CreateInstance(song.m_SongPath);
        song.m_SongInstance.start();
    }

    public static void StopMusic (Songs song)
    {
        song.m_SongInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
}
