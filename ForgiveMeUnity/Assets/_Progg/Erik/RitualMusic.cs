﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RitualMusic : MonoBehaviour
{
    bool triggered = false;
    private void OnTriggerEnter()
    {
        if (!triggered)
        {
            MusicHandler.Instance.ritual.Play();
            triggered = true;
        }
    }

    private void OnTriggerExit()
    {
        if (triggered)
        {
            MusicHandler.Instance.ritual.Stop();
            triggered = false;
        }
    }


    public void DestroyRitualMusic()
    {
        Destroy(this);
    }

}
