﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/OutlineShaderSmall"
{
    Properties
    {
        _OutLlineTex("Outline Texture", 2D) = "white"{}
        _OutlineColor("Outline Color", Color) = (1,1,1,1)
        _OutlineWidth("Outline Width", Range(0.01,0.3)) = 0.08



        _FadeStart("Fade Start Distance", Range(0.1,10.0)) = 1.1
        _FadeEnd("Fade End Distace", Range(0.1,10.0)) = 5.1
    }

        SubShader
        {
            Tags { "RenderType" = "Opaque" "Queue" = "Transparent" }
            LOD 200

                            Pass
                {
                Name "Outline"
                ZWrite Off
                Cull Back
                BlendOp Max
                Blend DstColor SrcColor
                CGPROGRAM

    #pragma vertex vert



    #pragma fragment frag



    #include "UnityCG.cginc"



            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 norm : NORMAL;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            float4 _OutlineColor;
            sampler2D _OutLlineTex;
            float _OutlineWidth;
            float _FadeStart;
            float _FadeEnd;


            v2f vert(appdata IN)
            {
                //float3 world = mul(unity_ObjectToWorld, float4(IN.norm, 0.0)).xyz;
               //float3 bigVertex = mul(unity_ObjectToWorld, float4(IN.vertex.xyz,1.0)).xyz;
                 //world  *= _OutlineWidth;
                //IN.norm = clamp(IN.norm,0,0.1);

                float3 vectorToWorld = mul(unity_ObjectToWorld, float4(IN.vertex.xyz, 1.0)).xyz;
                float viewDist = length(vectorToWorld - _WorldSpaceCameraPos);
                float fadeout = smoothstep(_FadeEnd, _FadeStart , viewDist);
                IN.vertex.xyz += clamp(clamp(IN.norm * _OutlineWidth,-1.5,1.5) * fadeout,-1, 1);
                IN.vertex.xyz *= 1 - step(_FadeEnd, viewDist);
                //bigVertex *= world;

               //IN.vertex.xyz -= _OutlineWidth;
               v2f OUT;

               OUT.pos = UnityObjectToClipPos(IN.vertex);
               OUT.uv = IN.uv;


               return OUT;
           }

           fixed4 frag(v2f IN) : SV_Target
           {
               float4 texColor = tex2D(_OutLlineTex, IN.uv);
               return texColor * _OutlineColor;
           }




           ENDCG
       }
        }
}
