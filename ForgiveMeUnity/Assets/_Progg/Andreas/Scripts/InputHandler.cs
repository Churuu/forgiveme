﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class InputHandler : MonoBehaviour
{
    private Vector2 m_XYMove;

/*
    [DllImport("user32.dll")]
    public static extern bool SetCursorPos(int X, int Y);
    [DllImport("user32.dll")]
    public static extern bool GetCursorPos(out Vector2Int pos);

    /*    [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public POINT(int x, int y)
            {
                this.X = x;
                this.Y = y;
            }

            public static implicit operator System.Drawing.Point(POINT p)
            {
                return new System.Drawing.Point(p.X, p.Y);
            }

            public static implicit operator POINT(System.Drawing.Point p)
            {
                return new POINT(p.X, p.Y);
            }
        }
        */

/*
    public class MouseOperations
    {
        [System.Flags]
        public enum MouseEventFlags
        {
            LeftDown = 0x00000002,
            LeftUp = 0x00000004,
            MiddleDown = 0x00000020,
            MiddleUp = 0x00000040,
            Move = 0x00000001,
            Absolute = 0x00008000,
            RightDown = 0x00000008,
            RightUp = 0x00000010
        }

        [DllImport("user32.dll", EntryPoint = "SetCursorPos")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetCursorPos(int X, int Y);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(out MousePoint lpMousePoint);

        [DllImport("user32.dll")]
        private static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        public static void SetCursorPosition(int X, int Y)
        {
            SetCursorPos(X, Y);
        }

        public static void SetCursorPosition(MousePoint point)
        {
            SetCursorPos(point.X, point.Y);
        }

        public static MousePoint GetCursorPosition()
        {
            MousePoint currentMousePoint;
            var gotPoint = GetCursorPos(out currentMousePoint);
            if (!gotPoint) { currentMousePoint = new MousePoint(0, 0); }
            return currentMousePoint;
        }

        public static void MouseEvent(MouseEventFlags value)
        {
            MousePoint position = GetCursorPosition();

            mouse_event
                ((int)value,
                 position.X,
                 position.Y,
                 0,
                 0)
                ;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MousePoint
        {
            public int X;
            public int Y;

            public MousePoint(int x, int y)
            {
                X = x;
                Y = y;
            }

        }

    }*/


    public delegate void CrouchHandler();
    public event CrouchHandler CrouchStateChange;

    public delegate void PickupHandler();
    public event PickupHandler PickupStateChange;
    public delegate void ReleaseHandler();
    public event ReleaseHandler ReleaseStateChange;

    public delegate void InventoryHandler();
    public event InventoryHandler InventoryStateChange;

    public delegate void JournalHandler();
    public event JournalHandler JournalStateChange;
    public delegate void InteractHandler(Vector3 mPos, bool isLeft);
    public event InteractHandler InteractStateChange;

    private Vector2 m_mouseAxis;

    public static InputHandler Instance;

    private bool m_paused = false, m_mousedown = false;


    // For non-delegate inputs, add the in the list before  crouch, for delegate inputs, add after crouch, add default values to LoadControls, if delegated, add it to delegateCall
    public enum KeyCallType { forward, backward, left, right, crouch, inventory, journal, pickup, inventoryInteract, mouseUp, mouseDown, mouseLeft, mouseRight };
    //public enum MousecontrollOverRides {mouseUp, mouseDown, mouseLeft, mouseRight};
    //private bool isKeyboardOverride = false;
    //private mouseOverrides[] m_mouseOverrideControlls = new mouseOverrides[System.Enum.GetValues(typeof(MousecontrollOverRides)).Length];

    private KeyBindHandler.CompoundInputStruct[] m_actualControlls = new KeyBindHandler.CompoundInputStruct[System.Enum.GetValues(typeof(KeyCallType)).Length];
    
    private SuplimentaryInputData[] m_suplimentaryInputData = new SuplimentaryInputData[System.Enum.GetValues(typeof(KeyCallType)).Length];



    int crounchInt, mouseupInt, pickupInt, keycalllenght;
    //bool leftSimulateClick, rightSimulateClick;

    private void LoadControlls()
    {
        //TODO: actually impliment a system for savign and loading controll-configs etc
        m_actualControlls[(int)KeyCallType.forward].keyCode = KeyCode.W;
        m_actualControlls[(int)KeyCallType.backward].keyCode = KeyCode.S;
        m_actualControlls[(int)KeyCallType.left].keyCode = KeyCode.A;
        m_actualControlls[(int)KeyCallType.right].keyCode = KeyCode.D;
        m_actualControlls[(int)KeyCallType.crouch].keyCode = KeyCode.LeftShift;
        m_actualControlls[(int)KeyCallType.inventory].keyCode = KeyCode.Tab;
        m_actualControlls[(int)KeyCallType.journal].keyCode = KeyCode.E;
        m_actualControlls[(int)KeyCallType.pickup].keyCode = KeyCode.Mouse0;
        m_actualControlls[(int)KeyCallType.inventoryInteract].keyCode = KeyCode.Mouse1;

        m_suplimentaryInputData[(int)KeyCallType.forward].PGM = PauseGoMode.No;
        m_suplimentaryInputData[(int)KeyCallType.backward].PGM = PauseGoMode.No;
        m_suplimentaryInputData[(int)KeyCallType.left].PGM = PauseGoMode.No;
        m_suplimentaryInputData[(int)KeyCallType.right].PGM = PauseGoMode.No;
        m_suplimentaryInputData[(int)KeyCallType.crouch].PGM = PauseGoMode.No;
        m_suplimentaryInputData[(int)KeyCallType.inventory].PGM = PauseGoMode.Both;
        m_suplimentaryInputData[(int)KeyCallType.journal].PGM = PauseGoMode.Both;
        m_suplimentaryInputData[(int)KeyCallType.pickup].PGM = PauseGoMode.Both;
        m_suplimentaryInputData[(int)KeyCallType.inventoryInteract].PGM = PauseGoMode.Yes;

        m_suplimentaryInputData[(int)KeyCallType.mouseUp].PGM = PauseGoMode.Both;
        m_suplimentaryInputData[(int)KeyCallType.mouseDown].PGM = PauseGoMode.Both;
        m_suplimentaryInputData[(int)KeyCallType.mouseLeft].PGM = PauseGoMode.Both;
        m_suplimentaryInputData[(int)KeyCallType.mouseRight].PGM = PauseGoMode.Both;


        m_actualControlls[(int)KeyCallType.mouseUp].keyCode = KeyCode.UpArrow;
        m_actualControlls[(int)KeyCallType.mouseDown].keyCode = KeyCode.DownArrow;
        m_actualControlls[(int)KeyCallType.mouseLeft].keyCode = KeyCode.LeftArrow;
        m_actualControlls[(int)KeyCallType.mouseRight].keyCode = KeyCode.RightArrow;
    }

    internal enum PauseGoMode { No, Yes, Both };

    internal struct SuplimentaryInputData
    {
        internal PauseGoMode PGM;
    }

    /*public struct mouseOverrides
    {
        internal KeyCode keyCode;
        internal KeyBindHandler.InputType inputType;
        internal MousecontrollOverRides overrideCode;
    }*/


    public KeyBindHandler.CompoundInputStruct[] GetCurrentControlls()
    {
        return m_actualControlls;
    }
   /* public bool getMousePointcontrollState()
    {
        return isKeyboardOverride;
    }*/

    public void SetGameControlls(KeyBindHandler.CompoundInputStruct[] GC)
    {
        m_actualControlls = GC;
        //isKeyboardOverride = keyOverrides;
       // leftSimulateClick = !(m_actualControlls[pickupInt].keyCode == KeyCode.Mouse0);
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }

        LoadControlls();


        

        
        //preload values
        crounchInt = (int)KeyCallType.crouch;
        mouseupInt = (int)KeyCallType.mouseUp;
        pickupInt = (int)KeyCallType.pickup;
        keycalllenght = System.Enum.GetValues(typeof(KeyCallType)).Length;
        for(int i = 0; i < keycalllenght; i++)
        {
            m_actualControlls[i].keyCall = (KeyCallType)System.Enum.GetValues(typeof(KeyCallType)).GetValue(i);
        }
        //leftSimulateClick = !(m_actualControlls[pickupInt].keyCode == KeyCode.Mouse0);
        //rightSimulateClick = m_actualControlls[(int)KeyCallType.inventoryInteract].keyCode == KeyCode.Mouse1;
    }

    /// <summary>
    /// "Call to switch game between frozen "paused" state and other states
    /// </summary>
    public void TogglePauseState()
    {
        m_paused = !m_paused;
    }

   /* private void LateUpdate()
    {
        provideMouseInput();
    }*/

    // Update is called once per frame
    void Update()
    {

        for (int i = crounchInt; i < mouseupInt; i++)
        {
            if (i != pickupInt || m_paused)
            {
                if (GetInputData(m_actualControlls[i].keyCall))
                {
                    //print("blipblobb");
                    //print((KeyCallType)System.Enum.GetValues(typeof(KeyCallType)).GetValue(i));
                    DelelgateCall(i);
                }
            }
            else ExceptionBlock();
        }

/*       if (isKeyboardOverride)
            provideMouseInput();*/

    }

    private void ExceptionBlock()
    {
        if (Input.GetKeyDown(m_actualControlls[(int)KeyCallType.pickup].keyCode) && PickupStateChange != null)
        {
            PickupStateChange();
            /*if (leftSimulateClick)
                if (m_mousedown)
                {
                    MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftDown);
                    m_mousedown = !m_mousedown;
                }
                else if (!m_mousedown)
                {
                    MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftUp);
                    m_mousedown = !m_mousedown;
                }*/
        }
        if (Input.GetKeyUp(m_actualControlls[(int)KeyCallType.pickup].keyCode) && ReleaseStateChange != null)
            ReleaseStateChange();

    }

    private void DelelgateCall(int i)
    {
        switch (i)
        {
            case (int)KeyCallType.crouch:
                if (CrouchStateChange != null) { CrouchStateChange(); }
                break;
            case (int)KeyCallType.inventory:
                if (InventoryStateChange != null) { InventoryStateChange(); }
                break;
           /* case (int)KeyCallType.journal:
                if (JournalStateChange != null) { JournalStateChange(); }
                break;*/
            case (int)KeyCallType.pickup:
                //print("clicky dicky");
                    /*if (leftSimulateClick)
                        if (m_mousedown)
                        {
                            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftDown);
                            m_mousedown = !m_mousedown;
                        }
                        else if (!m_mousedown)
                        {
                            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftUp);
                            m_mousedown = !m_mousedown;
                        }*/
                if (InteractStateChange != null) { InteractStateChange(Input.mousePosition, true); }
                break;
            case (int)KeyCallType.inventoryInteract:
                if (InteractStateChange != null) { InteractStateChange(Input.mousePosition, false); }
                break;
        }

    }

    private bool GetInputData(KeyCallType KCT)
    {
        if (m_paused && m_suplimentaryInputData[(int)KCT].PGM == PauseGoMode.No || !m_paused && m_suplimentaryInputData[(int)KCT].PGM == PauseGoMode.Yes)
            return false;

        if (m_actualControlls[(int)KCT].InputParseType == KeyBindHandler.InputType.Keyboard || (m_actualControlls[(int)KCT].InputParseType == KeyBindHandler.InputType.MouseButton))
            return CheckInput(KCT);

        else if (m_actualControlls[(int)KCT].InputParseType == KeyBindHandler.InputType.ScrollWheel)
            return GetScrollInput(KCT);

        return false;
    }

    private bool CheckInput(KeyCallType KCT)
    {
        //print("ffprrgg");
        if ((int)KCT < 7)
        {
            if (Input.GetKey(m_actualControlls[(int)KCT].keyCode))
                return true;
        }
        else
            if (Input.GetKeyDown(m_actualControlls[(int)KCT].keyCode) || Input.GetKeyUp(m_actualControlls[(int)KCT].keyCode))
            return true;
        return false;
    }


    private bool GetScrollInput(KeyCallType KCT)
    {
        float eval = Input.mouseScrollDelta.y;
        if (eval > 0 && m_actualControlls[(int)KCT].keyCode == KeyCode.Mouse1)
            return true;
        else if (eval < 0 && m_actualControlls[(int)KCT].keyCode == KeyCode.Mouse0)
            return true;
        else
            return false;
    }




    /// <summary>
    /// Returns the vector 2 mouse delta as last updated, and the nulls the value
    /// </summary>
    /// <returns></returns>
    public Vector2 getMouseDelta()
    {
        //return Vector2.one;
        Vector2 v = m_mouseAxis;
        m_mouseAxis = Vector2.zero;

        return v;

    }

    /*   public float getScrollDelta()
       {
           return Input.mouseScrollDelta.y;

       */




    /// <summary>
    /// Returns a Vector2 formated as a movement-delta, and resets the value of the delta
    /// </summary>
    /// <returns></returns>
    public Vector2 GetXYMove()
    {
        m_XYMove = Vector2.zero;
        if (!m_paused)
        {
            if (GetInputData(KeyCallType.forward))
                m_XYMove.x = 1;
            else if (GetInputData(KeyCallType.backward))
                m_XYMove.x = -1;

            if (GetInputData(KeyCallType.left))
                m_XYMove.y = 1;
            else if (GetInputData(KeyCallType.right))
                m_XYMove.y = -1;
        }
        return m_XYMove;
    }

    /*private void provideMouseInput()
    {
        Vector2Int m = Vector2Int.zero;
        if (GetInputData(KeyCallType.mouseUp))
            m.y = -1;
        else if (GetInputData(KeyCallType.mouseDown))
            m.y = 1;

        if (GetInputData(KeyCallType.mouseLeft))
            m.x = -1;
        else if (GetInputData(KeyCallType.mouseRight))
            m.x = 1;

        Vector2Int r;
        GetCursorPos(out r);
        r += m * 20;
        SetCursorPos((int)r.x, (int)r.y);
    }*/

    //Records input values and formats them into input-deltas
    private void OnGUI()
    {
        m_mouseAxis = Vector2.zero;
        m_mouseAxis = Event.current.delta;
    }

}
