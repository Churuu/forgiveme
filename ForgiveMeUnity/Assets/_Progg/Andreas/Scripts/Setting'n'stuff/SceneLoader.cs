﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private Button m_button;
    [SerializeField] private string m_sceneName;


    private void Start()
    {
        m_button.onClick.AddListener(delegate { try { SceneManager.LoadScene(m_sceneName); } catch { Debug.LogError(m_sceneName); } });
    }
}
