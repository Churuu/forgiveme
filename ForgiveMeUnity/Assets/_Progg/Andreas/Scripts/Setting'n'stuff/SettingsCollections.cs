﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using FMOD.Studio;
using FMOD;
public class SettingsCollections : MonoBehaviour
{

    private float m_secondsToResolutionReset = 0.2f;
    private float m_secondsSinceLastToggle = 0;


    private float _soundVolumeMaster, _soundVolumeMusic, _soundVolumeSFX, _soundVolumeDialog;
    private bool _isFullScreen, _keepNewResolution, _revertToOldResolution, _oldFullScreenMode;
    private Resolution _previousResolution;
    private readonly float RESET_RESOLUTION_CONFIRM_DURATION = 15, DECIMAL_TO_DECIBEL = 20, INVERSE_LOG = 10;
    [SerializeField] private Dropdown _resolutionDropdown;
    [SerializeField] private Toggle _fullScreenToggle;
    [SerializeField] private Text _masterVolumeText, _musicVolumeText, _sfxVolumeText, _dialogVolumeText, _secondMarkerText, _titleText;
    [SerializeField] private Slider _masterVolumeSlider, _musicVolumeSlider, _sfxVolumeSlider, _dialogVolumeSlider;
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private GameObject _confirmMenu;
    [SerializeField] private Image _mainSettings, _mainMenu;
    [SerializeField] private Camera _camera;
    [SerializeField] private GameObject _screenParent;

    Bus m_MasterBus, m_SFX, m_Music;
    float m_MasterVolume, m_SFXVolume, m_musicVolume;

    string MastervcaPath = "vca:/Master";
    FMOD.Studio.VCA Master;

    string SFXvcaPath = "vca:/SFX";
    static FMOD.Studio.VCA SFX;

    string MusicvcaPath = "vca:/Music";
    FMOD.Studio.VCA Music;


    private Image _currentSet;


    struct Settings
    {
        internal float _soundVolumeMaster, _soundVolumeMusic, _soundVolumeSFX, _soundVolumeDialog;
        internal bool _isFullScreen;
        internal Resolution _resolution;
    }
    Settings _oldSettings;
    Resolution[] _screenResolutions;











    void Start()
    {
        #region Faffy Setup Stuff
        Resolution currentResolution = GetActualResolution();
        _screenResolutions = Screen.resolutions;
        _previousResolution = currentResolution;

        _oldSettings = new Settings();
        _oldSettings._isFullScreen = Screen.fullScreen;
        _oldSettings._resolution = currentResolution;
        // _oldSettings._soundVolumeMaster = Mathf.Pow(INVERSE_LOG, (_audioMixer.GetVolumeValue("Master") / DECIMAL_TO_DECIBEL));



        Master = FMODUnity.RuntimeManager.GetVCA(MastervcaPath);

        SFX = FMODUnity.RuntimeManager.GetVCA(SFXvcaPath);
        Music = FMODUnity.RuntimeManager.GetVCA(MusicvcaPath);
        //Master.setVolume(volume);
        Master.getVolume(out m_MasterVolume);
        SFX.getVolume(out m_SFXVolume);
        Music.getVolume(out m_musicVolume);
        //m_MasterVolume = PlayerPrefs.GetFloat("MasterVolume");
        //m_MasterBus = FMODUnity.RuntimeManager.GetBus("Master");

        //m_SFXVolume = PlayerPrefs.GetFloat("SFXVolume");
        //m_SFX = FMODUnity.RuntimeManager.GetBus("SFXBus");

        //m_musicVolume = PlayerPrefs.GetFloat("MusicVolume");
        //m_Music = FMODUnity.RuntimeManager.GetBus("MusicBus");

        _oldSettings._soundVolumeMaster = Mathf.Pow(INVERSE_LOG, (m_MasterVolume / DECIMAL_TO_DECIBEL));

        _oldSettings._soundVolumeMusic = Mathf.Pow(INVERSE_LOG, m_musicVolume / DECIMAL_TO_DECIBEL);
        //_oldSettings._soundVolumeDialog = Mathf.Pow(INVERSE_LOG, (_audioMixer.GetVolumeValue("Dialog") / DECIMAL_TO_DECIBEL));
        _oldSettings._soundVolumeSFX = Mathf.Pow(INVERSE_LOG, (m_SFXVolume / DECIMAL_TO_DECIBEL));
        m_secondsToResolutionReset = RESET_RESOLUTION_CONFIRM_DURATION;
        #endregion

        _currentSet = _mainMenu;
        _ResolutionQuickSort(_screenResolutions, 0, _screenResolutions.Length - 1);
        _isFullScreen = Screen.fullScreen;
        _fullScreenToggle.isOn = _isFullScreen;
        _oldFullScreenMode = _isFullScreen;
        _fullScreenToggle.onValueChanged.AddListener(delegate {
            _isFullScreen = _fullScreenToggle.isOn; Screen.fullScreen = _isFullScreen;
            m_secondsSinceLastToggle = Time.realtimeSinceStartup; StartCoroutine(ChangeWindowMode()); ReCentre();
        });

        for (int i = 0; i < _screenResolutions.Length; i++)
        {

            _resolutionDropdown.options[i].text = ResolutionToString(_screenResolutions[i]);
            //Debug.Log(ResolutionToString(_screenResolutions[i]));
            _resolutionDropdown.value = i;
            if ((i != _screenResolutions.Length - 1))
                _resolutionDropdown.options.Add(new Dropdown.OptionData(_resolutionDropdown.options[i].text));
        }

        //Debug.Log(_screenResolutions.FindResolutionInArray(currentResolution));
        _resolutionDropdown.value = _screenResolutions.FindResolutionInArray(currentResolution);
        _resolutionDropdown.RefreshShownValue();
        _resolutionDropdown.onValueChanged.AddListener(delegate {
            Screen.SetResolution(_screenResolutions[_resolutionDropdown.value].width,
            _screenResolutions[_resolutionDropdown.value].height, _isFullScreen); m_secondsSinceLastToggle = Time.realtimeSinceStartup;
            StartCoroutine(ChangeResolution()); ReCentre();
        });

        _masterVolumeSlider.onValueChanged.AddListener(delegate { SetMasterVolume(_masterVolumeSlider); });
        _musicVolumeSlider.onValueChanged.AddListener(delegate { SetMusicVolume(_musicVolumeSlider); });
        _dialogVolumeSlider.onValueChanged.AddListener(delegate { SetDialogVolume(_dialogVolumeSlider); });
        _sfxVolumeSlider.onValueChanged.AddListener(delegate { SetSFXVolume(_sfxVolumeSlider); });
        //Debug.LogError("old slider value: "+ _gammaSlider.value); Debug.LogError("old alpha value: " + _postProcessor.colorGrading.settings.colorWheels.log.power.a);
        //Debug.LogError("new slider value: " + _postProcessor.colorGrading.settings.colorWheels.log.power.a.GammaToSlider());

        //    _gammaSlider.GammaToSlider(_postProcessor.GetSetting<ColorGrading>().gamma.value.w);
        // Debug.LogError(_gammaSlider.value);
        //      _gammaSlider.onValueChanged.AddListener(delegate { SetNewGamma(_gammaSlider); });
        //        _gammaSlider.GammaToSlider(_postProcessor.GetSetting<ColorGrading>().gamma.value.w);

        //Debug.Log(_gammaSlider.value); Debug.Log(_postProcessor.colorGrading.settings.colorWheels.log.power.a);
        _sfxVolumeSlider.value = Mathf.Pow(INVERSE_LOG, (m_SFXVolume / DECIMAL_TO_DECIBEL));
        _musicVolumeSlider.value = Mathf.Pow(INVERSE_LOG, (m_musicVolume / DECIMAL_TO_DECIBEL));
        _masterVolumeSlider.value = Mathf.Pow(INVERSE_LOG, (m_MasterVolume / DECIMAL_TO_DECIBEL));
        _dialogVolumeSlider.value = Mathf.Pow(INVERSE_LOG, (_audioMixer.GetVolumeValue("Dialog") / DECIMAL_TO_DECIBEL));
        //Debug.Log(_audioMixer.GetVolumeValue("Music", -80, 0));     

    }









    public void BackToMainMenu()
    {
        float positionDifference = Mathf.Abs(_mainSettings.transform.position.x - _mainMenu.transform.position.x);
        _screenParent.transform.position = new Vector3(_screenParent.transform.position.x + positionDifference, _screenParent.transform.position.y, _screenParent.transform.position.z);
        _currentSet = _mainMenu;
    }
    public void MoveToSettings()
    {
        float positionDifference = Mathf.Abs(_mainMenu.transform.position.x - _mainSettings.transform.position.x);

        _screenParent.transform.position = new Vector3(_screenParent.transform.transform.position.x - positionDifference, _screenParent.transform.position.y, _screenParent.transform.position.z);
        _currentSet = _mainSettings;
    }

    void SetMasterVolume(Slider volume)
    {
        _soundVolumeMaster = volume.value;
        _masterVolumeText.text = volume.SliderValueToPercentString();
        //_audioMixer.SetFloat("Master", volume.value != 0 ? (DECIMAL_TO_DECIBEL * Mathf.Log10(volume.value)) : -80);
        m_MasterBus.setVolume(volume.value != 0 ? (DECIMAL_TO_DECIBEL * Mathf.Log10(volume.value) - 80) : 0);
        //Debug.Log(_audioMixer.GetVolumeValue("Master"));
    }
    void SetMusicVolume(Slider volume)
    {
        _soundVolumeMusic = volume.value;
        _musicVolumeText.text = volume.SliderValueToPercentString();
        //_audioMixer.SetFloat("Music", volume.value != 0 ? (DECIMAL_TO_DECIBEL * Mathf.Log10(volume.value)) : -80);
        Music.setVolume(volume.value != 0 ? (DECIMAL_TO_DECIBEL * Mathf.Log10(volume.value) - 80) : 0);
    }
    void SetSFXVolume(Slider volume)
    {
        _soundVolumeSFX = volume.value;
        _sfxVolumeText.text = volume.SliderValueToPercentString();
        //_audioMixer.SetFloat("SFX", volume.value != 0 ? (DECIMAL_TO_DECIBEL * Mathf.Log10(volume.value)) : -80);
        SFX.setVolume(volume.value != 0 ? (DECIMAL_TO_DECIBEL * Mathf.Log10(volume.value) - 80) : 0);
    }
    void SetDialogVolume(Slider volume)
    {
        _soundVolumeDialog = volume.value;
        _dialogVolumeText.text = volume.SliderValueToPercentString();
        _audioMixer.SetFloat("Dialog", volume.value != 0 ? (DECIMAL_TO_DECIBEL * Mathf.Log10(volume.value) - 80) : 0);
    }


    public void ReCentre()
    {
        float positionDifference = /*Mathf.Abs(*/_currentSet.transform.position.x - _camera.transform.position.x;//);
        _screenParent.transform.position = new Vector3(_screenParent.transform.position.x - positionDifference, _screenParent.transform.position.y, _screenParent.transform.position.z);
        //_screenParent.transform.position = new Vector3(_currentSet.transform.position.x, _currentSet.transform.position.y, _screenParent.transform.position.z);
    }

    Resolution GetActualResolution()
    {
        Resolution currentResolution = new Resolution();
        currentResolution.width = Screen.width;
        currentResolution.height = Screen.height;
        return currentResolution;
    }
    Resolution GetSelectedResolution()
    {
        Resolution currentResolution = new Resolution();
        currentResolution.width = _screenResolutions[_resolutionDropdown.value].width;
        currentResolution.height = _screenResolutions[_resolutionDropdown.value].height;
        return currentResolution;
    }

    public void SetFullScreen(bool mode)
    {
        Screen.fullScreen = mode;
    }

    void _ResolutionQuickSort(Resolution[] data, int left, int right)
    {
        Resolution Pivot = data[left];
        int leftHold = left;
        int rightHold = right;
        while (leftHold < rightHold)
        {
            while (data[leftHold].width < Pivot.width && leftHold <= rightHold)
            {
                leftHold++;
            }
            while (data[rightHold].width > Pivot.width && rightHold >= leftHold)
            {
                rightHold--;
            }
            if (leftHold < rightHold)
            {
                Resolution tempLeft = data[leftHold];
                Resolution tempRight = data[rightHold];
                data[leftHold] = tempRight;
                data[rightHold] = tempLeft;
                if (data[rightHold].width == Pivot.width && data[leftHold].width == Pivot.width)
                {
                    leftHold++;
                }
            }
        }
        if (left < leftHold - 1)
        {
            _ResolutionQuickSort(data, left, leftHold - 1);
        }
        if (right > rightHold + 1)
        {
            _ResolutionQuickSort(data, rightHold + 1, right);
        }
    }

    string ResolutionToString(Resolution res)
    {
        return res.width + "x" + res.height + "  @ " + res.refreshRate + "HZ";
    }

    public void ButtonRevert()
    {
        _revertToOldResolution = true;
    }
    public void KeepNewResolution()
    {
        _keepNewResolution = true;
    }


    IEnumerator ChangeWindowMode()
    {
        //_secondsToResolutionReset.Reset();
        //      m_secondsSinceLastToggle = Time.realtimeSinceStartup;
        _titleText.text = "Keep new window mode?";
        _revertToOldResolution = false;
        _keepNewResolution = false;
        if (_confirmMenu != null)
        {
            _confirmMenu.SetActive(true);
            while (true)
            {
                if (_secondMarkerText != null)
                    _secondMarkerText.text = (m_secondsSinceLastToggle + m_secondsToResolutionReset - Time.realtimeSinceStartup).FloatToSecondsRemaining();
                if (m_secondsToResolutionReset < Time.realtimeSinceStartup - m_secondsSinceLastToggle || _revertToOldResolution)
                {
                    _revertToOldResolution = false;
                    _fullScreenToggle.isOn = _oldFullScreenMode;
                    //Debug.Log(_oldFullScreenMode);
                    Screen.fullScreen = _oldFullScreenMode;
                    break;
                }
                else if (_keepNewResolution)
                {
                    if (Application.isEditor)
                        _oldFullScreenMode = !_oldFullScreenMode;
                    else
                        _oldFullScreenMode = Screen.fullScreen;

                    _fullScreenToggle.isOn = _oldFullScreenMode;
                    _keepNewResolution = false;
                    break;
                }
                yield return new WaitForSecondsRealtime(1);


            }
            _confirmMenu.SetActive(false);
            StopAllCoroutines();
        }
    }
    IEnumerator ChangeResolution()
    {
        //m_secondsSinceLastToggle = Time.realtimeSinceStartup;
        _titleText.text = "Keep new resolution?";
        _revertToOldResolution = false;
        _keepNewResolution = false;
        if (_confirmMenu != null)
        {
            _confirmMenu.SetActive(true);
            while (true)
            {
                if (_secondMarkerText != null)
                    _secondMarkerText.text = (m_secondsSinceLastToggle + m_secondsToResolutionReset - Time.realtimeSinceStartup).FloatToSecondsRemaining();
                if (m_secondsToResolutionReset < Time.realtimeSinceStartup - m_secondsSinceLastToggle || _revertToOldResolution)
                {
                    _revertToOldResolution = false;
                    _resolutionDropdown.value = _screenResolutions.FindResolutionInArray(_previousResolution);
                    _resolutionDropdown.RefreshShownValue();
                    Screen.SetResolution(_previousResolution.width, _previousResolution.height, _isFullScreen);
                    break;
                }
                else if (_keepNewResolution)
                {
                    _previousResolution = GetSelectedResolution();
                    _keepNewResolution = false;
                    break;
                }
                yield return new WaitForSecondsRealtime(1);


            }
            _confirmMenu.SetActive(false);
            StopAllCoroutines();
        }
    }

}
