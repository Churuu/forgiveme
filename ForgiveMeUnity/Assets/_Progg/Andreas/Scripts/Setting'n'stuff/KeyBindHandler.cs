﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyBindHandler : MonoBehaviour
{
    //EventType m_mostRecentEventType;
    Event m_eventData;

    private DelegateEncapsulator m_currentSet;
    private bool m_primerSet;

    

    public enum InputType { MouseButton, ScrollWheel, Keyboard };

    private CompoundInputStruct[] m_newControlls;

    [SerializeField] private Button[] m_keyFields = new Button[System.Enum.GetValues(typeof(InputHandler.KeyCallType)).Length];
    [SerializeField] private Toggle m_useKeysToMoveMouseToggle;
    //private bool m_useKeysToMoveMouse = false;


    private CompoundInputStruct[] InputsNew = new CompoundInputStruct[System.Enum.GetValues(typeof(InputHandler.KeyCallType)).Length];
    private void OnValidate()
    {
        Button[] input = m_keyFields;
        m_keyFields = new Button[System.Enum.GetValues(typeof(InputHandler.KeyCallType)).Length];
        input.CopyTo(m_keyFields, 0);
    }
    private void Start()
    {
        m_newControlls = InputHandler.Instance.GetCurrentControlls();
        /*m_useKeysToMoveMouse = InputHandler.Instance.getMousePointcontrollState();
        for (int i = (int)InputHandler.KeyCallType.mouseUp; i < m_keyFields.Length; i++)
        {
            m_keyFields[i].interactable = m_useKeysToMoveMouse;
        }
        m_useKeysToMoveMouseToggle.isOn = m_useKeysToMoveMouse;*/
       // m_useKeysToMoveMouseToggle.onValueChanged.AddListener(delegate { for (int i = (int)InputHandler.KeyCallType.mouseUp; i < m_keyFields.Length; i++) { m_keyFields[i].interactable = !m_keyFields[i].interactable; } m_useKeysToMoveMouse = !m_useKeysToMoveMouse; });
        SetBoundText();

        DelegateEncapsulator[] encapsulatorArray = new DelegateEncapsulator[m_keyFields.Length];
        for (int i = 0; i < m_keyFields.Length; i++)
        {
            encapsulatorArray[i].ItemButton = m_keyFields[i];
            encapsulatorArray[i].KeyCall = (InputHandler.KeyCallType)System.Enum.GetValues(typeof(InputHandler.KeyCallType)).GetValue(i);
            encapsulatorArray[i].index = i;
        }

        foreach(DelegateEncapsulator entry in encapsulatorArray)
        {
            
            if (entry.ItemButton != null)
                entry.ItemButton.onClick.AddListener(delegate { MarkSelected(entry); });   
        }
    }

    private void SetBoundText()
    {

        for(int i = 0; i < m_keyFields.Length; i++)
        {
            m_keyFields[i].gameObject.GetComponentInChildren<Text>().text = m_newControlls[i].keyCode.ToString();
        }
        
        m_newControlls.CopyTo(InputsNew, 0);
    }

    public void ApplyControlls()
    {
        InputsNew.CopyTo(m_newControlls, 0);

        //InputHandler.Instance.SetGameControlls(m_newControlls, m_useKeysToMoveMouse);
    }

    public void RevertControlls()
    {
        m_newControlls = InputHandler.Instance.GetCurrentControlls();
        SetBoundText();
    }

    public struct DelegateEncapsulator
    {
        public Button ItemButton;
        public InputHandler.KeyCallType KeyCall;
        public int index;
    };

    public struct CompoundInputStruct
    {
        public KeyCode keyCode;
        public InputHandler.KeyCallType keyCall;
        public InputType InputParseType;

    };



    private void OnGUI()
    {
        m_eventData = Event.current;
        if (m_primerSet)
            ConfigureNewKey();
        
    }

    private void ConfigureNewKey()
    {
        if(Input.GetKey(KeyCode.LeftShift))
        {
            InputsNew[m_currentSet.index].keyCode = KeyCode.LeftShift;
            InputsNew[m_currentSet.index].keyCall = m_currentSet.KeyCall;
            InputsNew[m_currentSet.index].InputParseType = InputType.Keyboard;
            m_currentSet.ItemButton.gameObject.GetComponentInChildren<Text>().text = KeyCode.LeftShift.ToString();
            m_primerSet = false;
        }
        else if (Input.GetKey(KeyCode.RightShift))
        {
            InputsNew[m_currentSet.index].keyCode = KeyCode.RightShift;
            InputsNew[m_currentSet.index].keyCall = m_currentSet.KeyCall;
            InputsNew[m_currentSet.index].InputParseType = InputType.Keyboard;
            m_currentSet.ItemButton.gameObject.GetComponentInChildren<Text>().text = KeyCode.RightShift.ToString();
            m_primerSet = false;
        }
        else if (m_eventData.type == EventType.KeyDown)
        {
            InputsNew[m_currentSet.index].keyCode = m_eventData.keyCode;
            InputsNew[m_currentSet.index].keyCall = m_currentSet.KeyCall;
            InputsNew[m_currentSet.index].InputParseType = InputType.Keyboard;
            m_currentSet.ItemButton.gameObject.GetComponentInChildren<Text>().text = m_eventData.keyCode.ToString();
            m_primerSet = false;
        }
        else if(m_eventData.type == EventType.MouseDown && !m_eventData.isScrollWheel)
        {
            InputsNew[m_currentSet.index].keyCall = m_currentSet.KeyCall;
            InputsNew[m_currentSet.index].InputParseType = InputType.MouseButton;

            if (m_eventData.button == 0)
            {
                InputsNew[m_currentSet.index].keyCode = KeyCode.Mouse0;
                m_currentSet.ItemButton.gameObject.GetComponentInChildren<Text>().text = "LMB";
            }
            else if (m_eventData.button == 1)
            {
                InputsNew[m_currentSet.index].keyCode = KeyCode.Mouse1;
                m_currentSet.ItemButton.gameObject.GetComponentInChildren<Text>().text = "RMB";
            }
            else if (m_eventData.button == 2)
            {
                InputsNew[m_currentSet.index].keyCode = KeyCode.Mouse2;
                m_currentSet.ItemButton.gameObject.GetComponentInChildren<Text>().text = "MMB";
            }
            m_primerSet = false;
        }
        else if(m_eventData.type == EventType.ScrollWheel)
        {
            if (Input.mouseScrollDelta.y > 0)
            {
                InputsNew[m_currentSet.index].keyCode = KeyCode.Mouse1;
                InputsNew[m_currentSet.index].keyCall = m_currentSet.KeyCall;
                InputsNew[m_currentSet.index].InputParseType = InputType.ScrollWheel;
                m_currentSet.ItemButton.gameObject.GetComponentInChildren<Text>().text = "Scrollwheel forward";
                m_primerSet = false;
            }
            else
            {
                InputsNew[m_currentSet.index].keyCode = KeyCode.Mouse0;
                InputsNew[m_currentSet.index].keyCall = m_currentSet.KeyCall;
                InputsNew[m_currentSet.index].InputParseType = InputType.ScrollWheel;
                m_currentSet.ItemButton.gameObject.GetComponentInChildren<Text>().text = "Scrollwheel backwards";
                m_primerSet = false;
            }
        }
    }

    public void MarkSelected(DelegateEncapsulator type)
    {
        m_primerSet = true;
        m_currentSet = type;
    }



}
