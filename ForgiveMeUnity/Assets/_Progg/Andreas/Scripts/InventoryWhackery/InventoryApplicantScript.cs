﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryApplicantScript : MonoBehaviour
{

    [SerializeField] private Image m_preview = null;
    [SerializeField] private Text m_itemName = null;
    private InventoryObject m_base = null;
    private Text m_desciptionText = null;


    private void Start()
    {
        if (InventoryScript.Instance != null)
            InventoryScript.Instance.InventoryDepopulateCall += DeletosThis;
    }

    //OnEnable and OnDisable listens for events
    private void OnEnable()
    {
        if (InventoryScript.Instance != null)
            InventoryScript.Instance.InventoryDepopulateCall += DeletosThis;
    }

    private void OnDisable()
    {
        if (InventoryScript.Instance != null)
            InventoryScript.Instance.InventoryDepopulateCall -= DeletosThis;
    }


    /// <summary>
    /// Function for setting the  inventory "fields" of inventory entires
    /// </summary>
    /// <param name="_base">Teh inventoryObject that is to be displayed</param>
    /// <param name="descriptorText">Reference to the Inventory UI item desciption text object</param>
    public void Applicate(InventoryObject _base, Text descriptorText)
    {
        m_base = _base;
        m_desciptionText = descriptorText;
        m_preview.sprite = m_base.GetItemPreviewSprite();
        m_itemName.text = m_base.GetItemName();
    }

    /// <summary>
    /// Ensures that all items entries in the inventory are destroyed, in order to ensure there is no residue objects
    /// </summary>
    public void DeletosThis()
    {
        if (InventoryScript.Instance != null)
            InventoryScript.Instance.InventoryDepopulateCall -= DeletosThis;
        DestroyImmediate(this.gameObject);
    }

    /// <summary>
    /// Passes over relevant item properties to the item display components
    /// </summary>
    public void OnSelect()
    {
        m_desciptionText.text = m_base.GetInventoryItemDescription();
        InventoryObjectViewerScript.Instance.AssignViewerObject(m_base);
    }
}