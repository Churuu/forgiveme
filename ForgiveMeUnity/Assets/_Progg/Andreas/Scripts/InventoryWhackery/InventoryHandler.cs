﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InventoryHandler
{
	private static List<GameObject> inventory = new List<GameObject>();
	private static List<string> m_nameCheckList = new List<string>();

	public delegate void NewInventoryItem(string name);
	public static event NewInventoryItem NewInventoryItemEvent;

	/// <summary>
	/// Method for accessing an objects inventory information without
	/// actually fetching the object itself
	/// </summary>
	/// <param name="invIndex"></param>
	/// <returns>Inventory object</returns>
	public static InventoryObject AccessInventoryItem(int invIndex)
	{
		if (invIndex < 0 || invIndex >= inventory.Count)
		{
			Debug.Log("Can't access inventory item, index is out of bounds!");
			return null;
		}
		return inventory[invIndex].GetComponent<InventoryObject>();
	}

	/// <summary>
	/// Method for accessing an objects inventory information without
	/// actually fetching the object itself
	/// </summary>
	/// <param name="intObject"></param>
	/// <returns>Inventory object</returns>
	public static InventoryObject AccessInventoryItem(GameObject invObj)
	{
		int index = inventory.IndexOf(invObj);
		if (index < 0 || index >= inventory.Count)
		{
			Debug.Log("Item is not in inventory, can't access it!");
			return null;
		}
		return inventory[index].GetComponent<InventoryObject>();
	}

	/// <summary>
	/// Method for accessing an objects inventory information without
	/// actually fetching the object itself
	/// </summary>
	/// <param name="invName"></param>
	/// <returns>Inventory object</returns>
	public static InventoryObject AccessInventoryItem(string invName)
	{
		InventoryObject invObj = inventory.Find(x => x.name == invName).GetComponent<InventoryObject>();
		if (invObj == null)
			invObj = inventory.Find(x => x.GetComponent<InventoryObject>().GetItemName() == invName).GetComponent<InventoryObject>();
		if (invObj == null)
		{
			Debug.Log("Item is not in inventory, can't access it!");
			return null;
		}
		return invObj;
	}

	/// <summary>
	/// Method for removing and thus getting an object from the inventory
	/// </summary>
	/// <param name="invIndex"></param>
	/// <returns>The gameobject from the inventory.</returns>
	public static GameObject RemoveItemFromInventory(int invIndex)
	{
		if (invIndex < 0 || invIndex >= inventory.Count)
		{
			Debug.Log("Can't remove item from inventory, index is out of bounds!");
			return null;
		}
		GameObject invObject = inventory[invIndex];
		inventory.RemoveAt(invIndex);
		return invObject;
	}

	/// <summary>
	/// Method for removing and thus getting an object from the inventory
	/// </summary>
	/// <param name="invObj"></param>
	/// <returns>The gameobject from the inventory.</returns>
	public static GameObject RemoveItemFromInventory(GameObject invObj)
	{
		int index = inventory.IndexOf(invObj);
		if (index < 0 || index >= inventory.Count)
		{
			Debug.Log("Item is not in inventory, can't access it!");
			return null;
		}
		GameObject invObject = inventory[index];
		inventory.RemoveAt(index);
		return invObject;
	}

	/// <summary>
	/// Method for removing and thus getting an object from the inventory
	/// </summary>
	/// <param name="invName"></param>
	/// <returns>The gameobject from the inventory.</returns>
	public static GameObject RemoveItemFromInventory(string invName)
	{
		GameObject test = inventory.Find(x => x.name == invName).gameObject;
		if (test == null)
			test = inventory.Find(x => x.GetComponent<InventoryObject>().GetItemName() == invName);
		if (test == null)
		{
			Debug.Log("Item is not in inventory, can't access it!");
			return null;
		}
		inventory.Remove(test);
		return test;
	}

	/// <summary>
	/// Method for placing desired object at the placement position
	/// defined in the InventoryObject script
	/// </summary>
	/// <param name="invIndex"></param>
	public static void PlaceItemAtStaticPosition(int invIndex)
	{
		if (invIndex < 0 || invIndex >= inventory.Count)
		{
			Debug.Log("Can't remove item from inventory, index is out of bounds!");
		}
		GameObject invObject = RemoveItemFromInventory(invIndex);
		invObject.SetActive(true);
		invObject.transform.position =
			invObject.GetComponent<InventoryObject>().GetStaticPlacementObject().position;
		invObject.transform.rotation =
			invObject.GetComponent<InventoryObject>().GetStaticPlacementObject().rotation;
	}

	/// <summary>
	/// Method for placing desired object at the placement position
	/// defined in the InventoryObject script
	/// </summary>
	/// <param name="invIndex"></param>
	public static void PlaceItemAtStaticPosition(GameObject invObj)
	{
		int index = inventory.IndexOf(invObj);
		if (index < 0 || index >= inventory.Count)
		{
			Debug.Log("Item is not in inventory, can't access it!");
		}
		GameObject invObject = RemoveItemFromInventory(index);
		invObject.SetActive(true);
		invObject.transform.position =
			invObject.GetComponent<InventoryObject>().GetStaticPlacementObject().position;
		invObject.transform.rotation =
			invObject.GetComponent<InventoryObject>().GetStaticPlacementObject().rotation;
	}

	/// <summary>
	/// Method for storing a gameobject in the inventory
	/// </summary>
	/// <param name="invObject"></param>
	public static void StoreInventoryItem(GameObject invObject)
	{
		inventory.Add(invObject);

		if (NewInventoryItemEvent != null)
			NewInventoryItemEvent(invObject.GetComponent<InventoryObject>().GetItemName());

		invObject.GetComponent<InventoryObject>().OnPickup();

		invObject.SetActive(false);
	}

	/// <summary>
	/// Helper method for checking the current contents of the inventory
	/// </summary>
	public static void GetInventoryStatus()
	{
		for (int i = 0; i < inventory.Count; i++)
		{
			Debug.Log(inventory[i].name);
		}
	}
	/// <summary>
	/// Gets the size of the inventory list
	/// </summary>
	/// <returns></returns>
	public static int GetInventorySize()
	{
		return inventory.Count;
	}

	/// <summary>
	/// Method for checking if an object with a desired tag can be found
	/// </summary>
	/// <param name="tag"></param>
	/// <returns>True if object with tag can be found, else false</returns>
	public static bool CompareTag(string tag)
	{
		foreach (var item in inventory)
		{
			if (item.CompareTag(tag))
				return true;
		}
		return false;
	}

	/// <summary>
	/// Method for checking if an object with a desired name can be found
	/// </summary>
	/// <param name="name"></param>
	/// <returns>True if object with name can be found, else false</returns>
	public static bool CompareName(string name)
	{
		foreach (var item in inventory)
		{
			if (item.name == name)
				return true;
		}
		return false;
	}

	/// <summary>
	/// For getting the contents of the inventory, on request of Robin
	/// </summary>
	/// <returns>A list with the names of the objects in the inventory"</returns>
	public static List<string> GetInventoryNameList()
	{
		List<string> nameList = new List<string>();
		for (int i = 0; i < inventory.Count; i++)
		{
			nameList.Add(inventory[i].name);
		}
		return nameList;
	}

	/// <summary>
	/// For setting the contents of the inventory on loading a game
	/// </summary>
	/// <param name="nameList"></param>
	public static void SetInventoryNameChecklist(List<string> nameList)
	{
		m_nameCheckList = nameList;
	}

	/// <summary>
	/// For checking if an inventory item has already been picked up when loading a game
	/// </summary>
	/// <param name="name"></param>
	/// <returns>"True if a name can be found in the inventory checklist, else false"</returns>
	public static bool CompareCheckList(string name)
	{
		for (int i = 0; i < m_nameCheckList.Count; i++)
		{
			if (m_nameCheckList[i] == name)
			{
				return true;
			}
		}
		return false;
	}


	/// <summary>
	/// Method for checking if an object with a desired tag can be found, if so return first object found
	/// </summary>
	/// <param name="tag"></param>
	/// <returns>True if object with tag can be found, else false</returns>
	public static InventoryObject GetObjectWithTag(string tag)
	{
		foreach (var item in inventory)
		{
			if (item.CompareTag(tag))
				return item.GetComponent<InventoryObject>();
		}
		return null;
	}

	/// <summary>
	/// Method for checking if an object with a desired name can be found, if so return first object found
	/// </summary>
	/// <param name="name"></param>
	/// <returns>True if object with name can be found, else false</returns>
	public static InventoryObject GetObjectWithName(string name)
	{
		foreach (var item in inventory)
		{
			if (item.GetComponent<InventoryObject>().GetItemName() == name)
				return item.GetComponent<InventoryObject>();
		}
		return null;
	}

	/// <summary>
	/// Method for resetting/clearing the inventory, intended for use at the start of the game to clear the static inventory
	/// </summary>
	public static void ResetInventory()
	{
		for (int i = 0; i < inventory.Count; i++)
		{
			GameObject go = RemoveItemFromInventory(i);
			if (go == null)
				continue;
			GameObject.Destroy(go);
		}
		inventory.Clear();
	}
}