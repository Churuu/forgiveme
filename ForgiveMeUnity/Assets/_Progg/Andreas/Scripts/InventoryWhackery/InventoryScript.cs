﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryScript : MonoBehaviour
{
    [SerializeField, Range(0.01f, 1f)] private float m_minimumInventoryToggleDelay = 0.2f;
    private GameObject m_menuEntryPrefabInstance;

    private float m_lastToggle = 0;
    private GameObject m_inventoryParent, m_inventoryContent;
    private bool m_inventoryOpen;
    private Text m_desciptionText = null;
    [SerializeField] string m_notebookName;
    private bool m_journalPresent;
    private BookInventoryScript m_journalRef;
    [SerializeField] GameObject m_inventoryIlluminator;

    /* public delegate void InventoryPopulate(Sprite preview, string name);
     public event InventoryPopulate InventoryPopulateCall;*/
    public delegate void InventoryDepopulate();
    public event InventoryDepopulate InventoryDepopulateCall;
    public delegate void InventoryBookSwitch(InventoryObject InvObj);
    public event InventoryBookSwitch InventoryBookSwitchCall;

    public static InventoryScript Instance;

    //There may only be one
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }


    // Start is called before the first frame update
    void Start()
    {
        if (InputHandler.Instance != null)
        {
            InputHandler.Instance.InventoryStateChange += InventorySwitchState;
            InputHandler.Instance.JournalStateChange += InventoryOpenJournal;
        }

    //Fetches relevant objects within the prefab for later use
    m_inventoryParent = gameObject.GetComponentInChildren<Canvas>(true).gameObject;
        m_menuEntryPrefabInstance = gameObject.GetComponentInChildren<InventoryApplicantScript>(true).gameObject;
        m_inventoryContent = gameObject.GetComponentInChildren<ContentSizeFitter>(true).gameObject;
        m_inventoryOpen = m_inventoryParent.activeInHierarchy;

        if (m_inventoryParent != null)
        {
            m_inventoryParent.SetActive(!(m_inventoryOpen = m_inventoryParent.activeInHierarchy));
            m_desciptionText = GameObject.FindGameObjectWithTag("InventoryDescriptionText").GetComponent<Text>();
            m_inventoryParent.SetActive(!(m_inventoryOpen = m_inventoryParent.activeInHierarchy));
        }
    }

    //OnEnable and OnDisable listens for events
    private void OnEnable()
    {
        if (InputHandler.Instance != null)
        {
            InputHandler.Instance.InventoryStateChange += InventorySwitchState;
            InputHandler.Instance.JournalStateChange += InventoryOpenJournal;
        }
    }

    private void OnDisable()
    {
        if (InputHandler.Instance != null)
        {
            InputHandler.Instance.InventoryStateChange -= InventorySwitchState;
            InputHandler.Instance.JournalStateChange -= InventoryOpenJournal;
        }
    }

    private bool IsNotebookInInventory()
    {
        if (m_journalPresent)
            return true;
        else
        {
            m_journalRef = (BookInventoryScript)InventoryHandler.GetObjectWithName(m_notebookName);
            if (m_journalRef != null)
                return (m_journalPresent = true);
            else return false;
        }
    }

    private void InventoryOpenJournal()
    {
        if (IsNotebookInInventory())
        {
            if (m_inventoryOpen)
            {
                InventorySwitchState();
            }
            InventoryBookSwitchCall(m_journalRef);
        }
    }



    /// <summary>
    /// Enables and disables the inventory interface
    /// </summary>
    private void InventorySwitchState()
    {
        //Dont let a switch occur within the delay period from the last switch
        if (Time.realtimeSinceStartup - m_lastToggle > m_minimumInventoryToggleDelay)
        {
            //Sets a new delay period and changes game controll states
            m_lastToggle = Time.realtimeSinceStartup;
            InputHandler.Instance.TogglePauseState();
            MouseController.m_MouseInstance.LockUnlockCursor();
            //Activate/deactivate the inventory heirarchy if available
            if (m_inventoryParent != null)
                m_inventoryParent.SetActive(!(m_inventoryOpen = m_inventoryParent.activeInHierarchy));
            else
                Debug.LogWarning("m_inventoryParent set to null");

            //populate or depopulate the inventory
            if (!m_inventoryOpen)
            {
                PopulateInventory();
                m_inventoryIlluminator.SetActive(true);
            }
            else
                DepopulateInventory();
        }
        

    }

    /// <summary>
    /// Populates the inventory by filling it with inventory entries representing all item in the inventory 
    /// </summary>
    private void PopulateInventory()
    {
        //If there are items in the inventory list, loop over them until they are all spawned in, instatiate all as a copy of the menu entry prefab instance, and pass on the relevant references to it
        int inventorySize = InventoryHandler.GetInventorySize();
        if(inventorySize > 0)
        {

            for (int i  = 0; i < inventorySize; i++)
            {
                GameObject newInstance = Instantiate(m_menuEntryPrefabInstance, m_inventoryContent.transform);
                newInstance.SetActive(true);
                newInstance.GetComponent<InventoryApplicantScript>().Applicate(InventoryHandler.AccessInventoryItem(i),m_desciptionText);
            }
        }
    }

    /// <summary>
    /// Send out delegate call to tell the menu entry instances to destroy themselves
    /// </summary>
    private void DepopulateInventory()
    {
        if(InventoryDepopulateCall!=null)
            InventoryDepopulateCall();
        m_desciptionText.text = "Item Description text";

        m_inventoryIlluminator.SetActive(false);
    }
}
