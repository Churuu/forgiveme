﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAudioSignalRelayer : MonoBehaviour
{
    public enum AudioType { Example1, Example2, DefaultFallback};



    //Stores the Non-generic sound lookup refs
    [SerializeField] private AudioContainer[] audioContainers;


    //The whackery required to show a struct in the inspector, contains all the lookup refs
    [System.Serializable]
    internal struct AudioContainer
    {
        [SerializeField] public GameObject m_objectRef;
        [SerializeField] public AudioManager.PickupType m_audioPlaybackType;
    };

    //Adds relevant functions into relevant interaction delegates
    private void Start()
    {
        
        InventoryHandler.NewInventoryItemEvent += InventoryAudioRelay;
    }

    /// <summary>
    /// Compares picked up item name with the lookup refs, if found, transmit enum audio type, else send default fallback
    /// </summary>
    /// <param name="name">The inventory name of the object</param>
    private void InventoryAudioRelay(string name)
    {
        List<string> a = InventoryHandler.GetInventoryNameList();

        foreach (var entry in audioContainers)
        {
            if (entry.m_objectRef != null)
            {
                if (entry.m_objectRef.GetComponent<InventoryObject>().GetItemName().ToLower() == name.ToLower())
                {
                    AudioManager.Instance.pickupTypeDetermine(entry.m_audioPlaybackType);
                    return;
                }
            }
        }
        AudioManager.Instance.pickupTypeDetermine(AudioManager.PickupType.GenericPickup);
    }


}
