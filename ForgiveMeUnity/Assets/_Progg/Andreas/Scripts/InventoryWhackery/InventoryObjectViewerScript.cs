﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;
public class InventoryObjectViewerScript : MonoBehaviour
{

    [SerializeField, Range(0.01f, 1f)] private float m_minimumInventoryToggleDelay = 0.2f;
    [SerializeField, Range(0.01f, 1f)] private float m_minimumBookToggleDelay = 0.2f;
    [SerializeField, Range(0.01f, 1f)] private float m_minimumNoteBookToggleDelay = 0.2f;
    [SerializeField, Range(0.005f, 0.01f)] private float m_singleScrollIncrementSize = 0.01f;

    static public InventoryObjectViewerScript Instance;
    private GameObject m_viewModdle, m_bookModdle;
    private Mesh m_iMesh;
    [SerializeField] private GameObject go = null;
    [SerializeField] private GameObject m_bookGo = null;
    [SerializeField] private GameObject m_inventoryReadTextObj = null;
    [SerializeField] private GameObject m_turnText;
    private Text m_pageText;
    private Image m_pageImage;

    private bool m_currentState = false, m_activeState = false, m_desyncBlock = false;
    private float m_lastToggle = 0 , m_lastBookToggle = 0, m_lastNoteBookToggle = 0;
    private Image m_bounds;
    private Camera m_cam;
    private Vector3[] m_screenBounds = new Vector3[4];
    private Vector2 m_controll, m_lastMouseControll;
    private Vector3 m_itemRot = Vector3.zero;
    private Vector3 m_itemBounds = Vector3.zero;
    private int m_currentPage;

    

    private InventoryObject m_inventoryObj;
    private bool m_hasInspectorText = false, m_textOpen = false;
    private string[] m_inspectedTextPages;
    private Sprite m_orig;
    bool isOverride = false;
    //maybe concider changing this to a dunamic implementation later
    private const float MAX_BOUND_SIZE = 0.09f;

    private void Awake()
    {
        //ensures per-scene singleton access for ease of reach
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    void Start()
    {
        if (InventoryScript.Instance != null)
        {
            InventoryScript.Instance.InventoryDepopulateCall += DeletosThis;
            //InventoryScript.Instance.InventoryBookSwitchCall += BookCall;
        }
        //Find camera, interact area bounds and calculate screenspace bonds for mouse pointer
        m_cam = FindObjectOfType<Camera>();
        m_pageImage = m_inventoryReadTextObj.GetComponent<Image>();
        m_pageText = m_inventoryReadTextObj.GetComponentInChildren<Text>(true);
        m_orig = m_pageImage.sprite;
        m_bounds = gameObject.GetComponent<Image>();
        m_bounds.rectTransform.GetWorldCorners(m_screenBounds);

        for (int i = 0; i < m_screenBounds.Length; i++)
            m_screenBounds[i] = m_cam.WorldToScreenPoint(m_screenBounds[i]);
    }

    //OnEnable and OnDisable listens for events
    private void OnEnable()
    {
        if (InventoryScript.Instance != null)
        {
            InventoryScript.Instance.InventoryDepopulateCall += DeletosThis;
            //InventoryScript.Instance.InventoryBookSwitchCall += BookCall;
        }
        if (InputHandler.Instance != null)
            InputHandler.Instance.InteractStateChange += getInOutBounds;
    }

    private void OnDisable()
    {
        if (InventoryScript.Instance != null)
        {
            InventoryScript.Instance.InventoryDepopulateCall -= DeletosThis;
            //InventoryScript.Instance.InventoryBookSwitchCall -= BookCall;
        }
        if (InputHandler.Instance != null)
            InputHandler.Instance.InteractStateChange -= getInOutBounds;
    }

    private void Update()
    {
        //Run code if the inventory object interact state is set to true
        if(m_activeState && m_viewModdle != null)
        {

            m_lastMouseControll = m_controll;
            //fetch mouse movement delta
            m_controll = InputHandler.Instance.getMouseDelta();

            if (m_controll != m_lastMouseControll)
            {
                //Modify stored retation bu mouse movement delta
                m_itemRot.x += m_controll.y;
                m_itemRot.y += -m_controll.x;

                //convert stored rotation to quaternion and apply as new rotation
                m_viewModdle.transform.localRotation = Quaternion.Euler(new Vector3(m_itemRot.x, 0 , 0));
                go.transform.localRotation = Quaternion.Euler(new Vector3(0, m_itemRot.y, 0));
            }
            //float scrolldelta = InputHandler.Instance.getScrollDelta();
            //Vector3 zoom = new Vector3(m_singleScrollIncrementSize * scrolldelta, m_singleScrollIncrementSize * scrolldelta, m_singleScrollIncrementSize * scrolldelta);
            //print(scrolldelta + " " + zoom);
            //if (m_itemBounds.x + zoom.x < MAX_BOUND_SIZE && m_itemBounds.y + zoom.y < MAX_BOUND_SIZE && m_itemBounds.z + zoom.z < MAX_BOUND_SIZE)
            //{
            //    print("brr");
            //    m_itemBounds += zoom;
            //    m_viewModdle.transform.localScale = m_itemBounds;
            //}
        }
    }


    /// <summary>
    /// Set a item to be viewed within the inventory
    /// </summary>
    /// <param name="invObj">Inventory object to be passed over</param>

    public void AssignViewerObject(InventoryObject invObj)
    {
        //If there is already an object being viewed, destroy it first
        if (m_viewModdle != null)
            DeletosThis();

        //Initial item instancing and scaling to ensure object appears corrctly
        m_inventoryObj = invObj;
        m_iMesh = invObj.GetItemPreviewMesh();
        m_viewModdle = Instantiate(invObj.GetItemPreviewObject(), go.transform);
        m_viewModdle.SetActive(true);
        m_turnText.SetActive(true);
        m_viewModdle.tag = "Untagged";
        m_viewModdle.transform.localPosition = Vector3.zero;
        m_viewModdle.transform.localScale = new Vector3(100f, 100f, 100f);
        m_itemRot =  Vector3.zero;
        m_inventoryObj.SetObjectTextAsRead();
        m_viewModdle.transform.localRotation = Quaternion.Euler(new Vector3(m_itemRot.x, 0, 0));
        go.transform.localRotation = Quaternion.Euler(new Vector3(0, m_itemRot.y, 0));
        m_hasInspectorText = m_inventoryObj.GetInventoryTextTrue();
        if (m_hasInspectorText)
            m_inspectedTextPages = m_inventoryObj.GetInventoryText();
        
        //Fetches the objects "real size", measured in units, and checks if it exceeds the maximum allowed size in any axis

        m_itemBounds =  (Vector3.Scale(m_iMesh.bounds.size, m_viewModdle.transform.localScale));
        
        float OverstepVal = 0;
        
        if(m_itemBounds.x > MAX_BOUND_SIZE)
        {
            OverstepVal = m_itemBounds.x;
        }
        if (m_itemBounds.y > MAX_BOUND_SIZE && m_itemBounds.y > OverstepVal)
        {
            OverstepVal = m_itemBounds.y;
        }
        if (m_itemBounds.z > MAX_BOUND_SIZE && m_itemBounds.z > OverstepVal)
        {
            OverstepVal = m_itemBounds.z;
        }
        
        //If the maximum size is exceeded, makes sure that object is uniformly scaled down to fit within item preview area (more or less)
        //WARNING: Method doesnt work for non-uniform objects, objects with extremely tiny meshes or objects without unitary meshes, and will not appear correctly in item preview

        if (OverstepVal != 0)
        {
            OverstepVal =  MAX_BOUND_SIZE * (1 / (OverstepVal - MAX_BOUND_SIZE));

            m_viewModdle.transform.localScale = Vector3.Scale(m_viewModdle.transform.localScale, new Vector3(OverstepVal, OverstepVal, OverstepVal));
            m_itemBounds = (Vector3.Scale(m_iMesh.bounds.size, m_viewModdle.transform.localScale));
        }

        if(m_inventoryObj.IsThisInteractable())
        {
            ((FungusInteractiveInventoryObjectScript)m_inventoryObj).GetControllFlowchart().SetActive(true);
        }

    }

    /// <summary>
    /// Open or close the laura book note book by pressing E
    /// </summary>
    /// <param name="m_book">Ref of book being passed from InventoryScript</param>
    private void BookCall(InventoryObject m_book)
    {
        //Because it overrides other book toggles, make sure that the toggle is off cooldown before running hte code block to stop switch spam
        if (Time.realtimeSinceStartup - m_lastNoteBookToggle > (m_minimumNoteBookToggleDelay))
        {
            //If the assigned viewer object isnt already the book, set it to the book
            if (m_inventoryObj != m_book)
                AssignViewerObject(m_book);

            //Toggles the override, calls the fucntion to get the book proper to appear, then toggles the override back off
            isOverride = true;
            getInOutBounds(Vector3.zero, false);
            isOverride = false;

            //Sets a new timeref for the cooldown
            m_lastNoteBookToggle = Time.realtimeSinceStartup;
        }
    }


    /// <summary>
    /// This does a lot of things these days
    /// </summary>
    /// <param name="mousecontroll">The mouse pointer screen position</param>
    private void getInOutBounds(Vector3 mousecontroll, bool isLeft)
    {
        //Bool per-run through of wether the state of the book open-ness changed
        bool runSet = false;

        //Closes the book no matter where on the screen right-click happens
        if (!isLeft && m_bookModdle != null && (Time.realtimeSinceStartup - m_lastBookToggle > (m_minimumBookToggleDelay)))
        {
            //marks change happened, destroy book, 
            //Uhhh wait what the fuck is the purpose of this, i am so confused, does it work???? no??? help??? 
            //okay adding !m_desyncBlock as a conditional bypass in the activate block seems to have worked... somehow
            runSet = true;
            Destroy(m_bookModdle);
            //disable fingus logic
            ((BookInventoryScript)m_inventoryObj).GetControllFlowchart().SendFungusMessage("close");
            //Changed active states of the objects, conditionally
            if (!m_viewModdle.activeInHierarchy)
            {
                m_viewModdle.SetActive(!m_viewModdle.activeInHierarchy);
                m_turnText.SetActive(m_viewModdle.activeInHierarchy);
            }

            //if the book was enabled, set desync block to true,  okay but isnt the whole purpose of this that that it only happens when the moddle is deactivated and the book is active?
            //Do not touch you fool
            if (m_viewModdle.activeInHierarchy)
                m_desyncBlock = true;

            //Resets the timer to current time as ref point
            m_lastBookToggle = Time.realtimeSinceStartup;
        }
        //if the mousie is within the interact bounds, or the mouse has been released. pass through
        if ((((mousecontroll.x > m_screenBounds[0].x && mousecontroll.y > m_screenBounds[0].y && mousecontroll.x < m_screenBounds[2].x && mousecontroll.y < m_screenBounds[2].y)) || m_currentState) || isOverride)
        {
            //Check to make sure that the text change doesnt happen in a spammy manner where its hard to controll the end state
            if (Time.realtimeSinceStartup - m_lastToggle < (m_minimumInventoryToggleDelay) && m_currentState && m_hasInspectorText && !isLeft || isOverride)
            {
                //if it is a notebook and the timer isnt in a block state or desyncBlock isnt triggered, allow passage/bypass
                if (m_inventoryObj.IsThisNotebook() && ((Time.realtimeSinceStartup - m_lastBookToggle > (m_minimumBookToggleDelay)) || !m_desyncBlock) || isOverride)
                {
                    //Set the fungus controll flowchart to active state, starting up the flow logic
                    ((BookInventoryScript)m_inventoryObj).GetControllFlowchart().SendFungusMessage("open");
                    //Fuckign hate this, tbh, but whatever. If there isnt already a bookmoddle and it wasnt just destroyed, allow passage
                    if (m_bookModdle == null && !runSet)
                    {
                        //Unset desyncNlock, instantiate new book moddle at position and set localpos to 0
                        m_desyncBlock = false;
                        m_bookModdle = Instantiate(((BookInventoryScript)m_inventoryObj).GetOpenBookObject(), m_bookGo.transform);
                        m_bookModdle.SetActive(true);
                        m_bookModdle.tag = "Untagged";
                        m_bookModdle.transform.localPosition = Vector3.zero;
                        m_bookModdle.transform.localRotation = Quaternion.Euler(((BookInventoryScript)m_inventoryObj).GetSpawnRot());
                        m_bookModdle.transform.localScale += (Vector3.one * ((BookInventoryScript)m_inventoryObj).GetScaleChange());
                    }
                    //else if there is a bookmoddle and it wasnt just destroyed (????) destroy the book
                    else if(m_bookModdle != null && !runSet)
                        Destroy(m_bookModdle);

                    //updfate the lastbooktoggle timeref
                    m_lastBookToggle = Time.realtimeSinceStartup;
                }
                //otherwise if this isnt a book simply run the standard code for showing a text
                else if(!m_inventoryObj.IsThisNotebook())
                {
                    m_pageImage.sprite = m_inventoryObj.GetBackgroundSpriteTextInventory();
                    //m_pageImage.SetNativeSize();

                    //sets current page to th estart of the text, then activates loads in the page to the text
                    m_currentPage = 0;
                    m_inventoryReadTextObj.SetActive(!(m_textOpen = m_inventoryReadTextObj.activeInHierarchy));
                    m_pageText.text = m_inspectedTextPages[m_currentPage];
                }
                //If the object isnt a book or if there isnt a desycblock, allow passage
                if ((!m_inventoryObj.IsThisNotebook() || !m_desyncBlock))
                {
                    m_viewModdle.SetActive(!m_viewModdle.activeInHierarchy);
                    m_inventoryObj.SetInventoryTextAsRead();
                    m_turnText.SetActive(m_viewModdle.activeInHierarchy);
                    //if (m_bookModdle != null)
                    //    print(m_bookModdle.activeSelf + " " + m_viewModdle.activeSelf + " " + m_desyncBlock + " " + runSet);
                    //else
                    //    print("false " + m_viewModdle.activeSelf + " " + m_desyncBlock + " " + runSet);
                }
            }
            //If the last interact with the windows is beyond the minimum delay, or if mouse is released, pass through
            if ((Time.realtimeSinceStartup - m_lastToggle > m_minimumInventoryToggleDelay || m_currentState) && isLeft)
            { 
            //change the active state of the mouse (ie wether it is held down or released), and reset the cooldown
            //toggle wether the object will be rotated or not
            m_activeState = !m_activeState;
            }
            m_currentState = !m_currentState;
            m_lastToggle = Time.realtimeSinceStartup;
        }
    }

    /// <summary>
    /// Removes the spawned in game object and resets item specific persisten variables
    /// </summary>
    public void DeletosThis()
    {
        m_turnText.SetActive(false);
        Destroy(m_viewModdle);
        Destroy(m_bookModdle);
        m_itemRot = Vector3.zero;
        m_itemBounds = Vector3.zero;
        m_iMesh = null;
        m_hasInspectorText = false;
        m_inspectedTextPages = null;
        m_inventoryObj = null;
        m_inventoryReadTextObj.SetActive(false);
        m_pageImage.sprite = m_orig;
        m_desyncBlock = false;
    }
}
