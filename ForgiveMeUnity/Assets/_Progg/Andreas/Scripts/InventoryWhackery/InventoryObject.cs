﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;

public class InventoryObject : MonoBehaviour
{

    [SerializeField] protected string m_itemName = "";
    [SerializeField] protected Sprite m_itemSprite = null;
    [SerializeField] [TextArea] protected string m_inventoryDescription = "";
    //[SerializeField] [TextArea] private string m_pickupPrompt = "";
    [SerializeField] protected bool m_doesHaveObjectReadableText = false;
    [SerializeField] protected bool m_doesHaveReadableText = false;
    [SerializeField] [TextArea] protected string[] m_readableText;
    [SerializeField] protected Sprite m_inventoryReadTextBackground = null;
    [SerializeField] protected GameObject m_itemPreviewObject;
    [SerializeField] protected Transform m_placementPosition = null;
    [SerializeField] protected HighlightController m_highlightController;
    protected Mesh m_itemMesh;
    protected bool m_hasBeenRead = false, m_isNotebook = false, m_isInteractionable = false, m_hasBeenViewed = false;


    [SerializeField] action m_Action;

    public enum action
    {
        None,
        StartRitual,
        AdvancePhase,
        ShowSymbols,
        StopRadio
    }

    private void Awake()
    {
        //If no special overrides have been added, fetch preveiobject and related mesh from the object the item is assigned onto
        if (m_itemPreviewObject == null)
            m_itemPreviewObject = gameObject;

        if (m_itemPreviewObject != null) //&& m_itemPreviewObject.GetComponent<MeshFilter>() != null)
            m_itemMesh = m_itemPreviewObject.GetComponent<MeshFilter>().mesh;
    }

    void Start()
    {
        CheckIfInSavedInventory();
    }

    #region SavingStuff
    void CheckIfInSavedInventory()
    {
        if (InventoryHandler.CompareCheckList(gameObject.name))
        {
            InventoryHandler.StoreInventoryItem(gameObject);
        }
    }
    #endregion

    /// <summary>
    /// Returns the item preview object
    /// </summary>
    /// <returns></returns>
    public GameObject GetItemPreviewObject()
    {
        return m_itemPreviewObject;
    }
    /// <summary>
    /// Returns the item preview object mesh
    /// </summary>
    /// <returns></returns>
    public Mesh GetItemPreviewMesh()
    {
        return m_itemMesh;
    }

    /// <summary>
    /// Method for getting the full description of an item,
    /// intended for use in the UI
    /// </summary>
    /// <returns>Full item description</returns>
    public string GetInventoryItemDescription()
    {
        return m_inventoryDescription;
    }
    /// <summary>
    /// Returns the object inventory display name
    /// </summary>
    /// <returns></returns>
    public string GetItemName()
    {
        return m_itemName;
    }

    /// <summary>
    /// Can this object be inspected to get additional text
    /// </summary>
    /// <returns></returns>
    public bool GetInventoryTextTrue()
    {
        return m_doesHaveReadableText;
    }


    /// <summary>
    /// If the item has readable "inventory text", returns wether or not it has been read, otherwise always returns false
    /// </summary>
    /// <returns></returns>
    public bool HasInventoryTextBeenRead()
    {
        if (m_doesHaveReadableText)
            return m_hasBeenRead;
        else
            return false;
    }

    /// <summary>
    /// Does this object have text that can be read on its surface
    /// </summary>
    /// <returns></returns>
    public bool GetHasObjectTextTrue()
    {
        return m_doesHaveObjectReadableText;
    }

    /// <summary>
    /// Has the text on this object been read
    /// </summary>
    /// <returns></returns>
    public bool HasObjectTextBeenRead()
    {
        if (m_doesHaveObjectReadableText)
            return m_hasBeenViewed;
        else
            return false;
    }
    /// <summary>
    /// The text on this objects surface has been read now
    /// </summary>
    public void SetObjectTextAsRead()
    {
        m_hasBeenViewed = true;
    }


    /// <summary>
    /// Only returns true on Lauras notebook (script)
    /// </summary>
    /// <returns></returns>
    public bool IsThisNotebook()
    {
        return m_isNotebook;
    }

    public bool IsThisInteractable()
    {
        return m_isInteractionable;
    }

    /// <summary>
    /// sets m_hasBeenRead to true
    /// </summary>
    public void SetInventoryTextAsRead()
    {
        m_hasBeenRead = true;
    }

    /// <summary>
    /// Returns th3e text for the item, if applicable
    /// </summary>
    /// <returns></returns>
    public string[] GetInventoryText()
    {
        return m_readableText;
    }

    /// <summary>
    /// Returns the sprite that is to be the background to the text in the inventory on inspect
    /// </summary>
    /// <returns></returns>
    public Sprite GetBackgroundSpriteTextInventory()
    {
        return m_inventoryReadTextBackground;
    }

    /// <summary>
    /// REturns the items assigned inventory preveiw sprite
    /// </summary>
    /// <returns></returns>
    public Sprite GetItemPreviewSprite()
    {
        return m_itemSprite;
    }

    /// <summary>
    /// Method for fetching the text-prompt when picking up
    /// an inventory object
    /// </summary>
    /// <returns>Pickup prompt</returns>
    /*public string GetPickupPrompt()
	{
		return m_pickupPrompt;
	}
	*/
    /// <summary>
    /// Method for getting an objects pre-determined position
    /// and rotation
    /// </summary>
    /// <returns>Pre-determined placement object</returns>
    public Transform GetStaticPlacementObject()
    {
        return m_placementPosition;
    }

    public void LoadGameObjectIntoInventory(List<string> names)
    {
        gameObject.SetActive(true);
        foreach (var name in names)
        {
            if (name == GetItemName())
            {
                InventoryHandler.RemoveItemFromInventory(gameObject);
                InventoryHandler.StoreInventoryItem(gameObject);
                gameObject.SetActive(false);
            }
        }
    }

    public void OnPickup()
    {
        if (m_highlightController != null)
			m_highlightController.SwitchToRegular();
        if (m_Action == action.StartRitual)
        {
            FindObjectOfType<Ritual>().StartRadio();
        }
        else if (m_Action == action.AdvancePhase)
        {
			ItemProgressionHandler.Instance.AdvancePhase();
		}
        else if(m_Action == action.ShowSymbols)
        {
            FindObjectOfType<ExamineSymbols>().ActivatePuzzle();
        }
        else if (m_Action == action.StopRadio)
        {
			ItemProgressionHandler.Instance.AdvancePhase();
			FindObjectOfType<RadioManager>().StopRadio();
		}
    }
}
