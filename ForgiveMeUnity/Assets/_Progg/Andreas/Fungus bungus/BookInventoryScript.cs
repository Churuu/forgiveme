﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class BookInventoryScript : InventoryObject
{
    [SerializeField] protected Flowchart m_objectControllFlowchart = null;
    [SerializeField] protected GameObject m_bookOpenModdle = null;
    [SerializeField] protected Vector3 m_spanwRot;
    [SerializeField] protected float m_scaleChange = 0;

    // Start is called before the first frame update
    void Start()
    {
        m_isNotebook = true;
    }

    /// <summary>
    /// Return this books fungus control chart object
    /// </summary>
    /// <returns></returns>
    public Flowchart GetControllFlowchart()
    {
        return m_objectControllFlowchart;
    }

    public GameObject GetOpenBookObject()
    {
        return m_bookOpenModdle;
    }

    public Vector3 GetSpawnRot()
    {
        return m_spanwRot;
    }

    public float GetScaleChange()
    {
        return m_scaleChange;
    }
}
