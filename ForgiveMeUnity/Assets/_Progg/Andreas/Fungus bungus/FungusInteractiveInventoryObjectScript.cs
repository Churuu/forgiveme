﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FungusInteractiveInventoryObjectScript : InventoryObject
{
    [SerializeField] protected GameObject m_objectControllFlowchart = null;
    [SerializeField] protected float FungusActivateDelay = 0;
    private void Start()
    {
        m_isInteractionable = true;
    }
    //uhh guess this is a waste of time then???

    /// <summary>
    /// Return this books fungus control chart object
    /// </summary>
    /// <returns></returns>
    public GameObject GetControllFlowchart()
    {
        return m_objectControllFlowchart;
    }

}
