﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;

public class SymbolSound : MonoBehaviour
{

    [EventRef]
    public string m_SymbolHummingEvent;
    public EventInstance m_SymbolHumming;

    // Start is called before the first frame update
    void Start()
    {
        RuntimeManager.CreateInstance(m_SymbolHummingEvent);
        
    }


}
