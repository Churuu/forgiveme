﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using FMOD.Studio;
using FMODUnity;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

public class RadioManager : MonoBehaviour
{
	float m_RadioFrequency = 0.81f;
	float m_RadioClarity = 0.44f;
	public float sensitivity;
	public Text Fransradio, m_radioClarity;
	public GameObject m_RadioPrompt;

	[EventRef]
	public string m_RadioKnobEvent;
	public EventInstance m_RadioKnob;

	[SerializeField] private CinemachineVirtualCamera m_radioCam;
	[SerializeField] private GameObject m_radioKnob1, m_radioKnob2, m_player, m_playerCam;
	private bool m_interactionActive, m_isControllingKnob1, m_isControllingKnob2;
	private float m_lastRadioFrequency = 0.81f, m_lastRadioClarity = 0.44f;
	[SerializeField] private GameObject m_knobText1, m_knobText2;
	[Tooltip("The min- & max-frequency to check between for correct frequency")]
	[SerializeField] private float m_minFreq, m_maxFreq;
	[Tooltip("How long before the correct frequency is 'detected'")]
	[SerializeField] private float frequencyCheckDuration;
	[SerializeField] private Flowchart m_flowchart;

	private bool m_advancedPhase;
	private float m_freqCheckCounter = 0f;
	private FirstPersonController m_fpsController;
	private CinemachineVirtualCamera m_virtualCam;
	private BoxCollider m_collider;
	private Vector2 m_mouseOld;

	void Update()
	{
		if (m_interactionActive)
		{
			CheckForPreciseFrequency();

			if (m_isControllingKnob1)
			{
				if (!CheckMouseStillOnButton())
				{
					ClampOutsideButton();
					return;
				}
				Vector2 m_mouseCurrent = Input.mousePosition;

				Vector2 mouseChange = (m_mouseCurrent - m_mouseOld).normalized;
				if (m_mouseOld != Vector2.zero)
				{
					m_RadioFrequency += mouseChange.x * sensitivity;
					Vector3 rotation = m_radioKnob1.transform.localEulerAngles;
					rotation.z += mouseChange.x * sensitivity;
					m_radioKnob1.transform.localRotation = Quaternion.Euler(rotation);
				}

				m_RadioFrequency = Mathf.Clamp(m_RadioFrequency, 0.0f, 1.0f);
				m_RadioKnob.setParameterByName("Radio_Frequency", m_RadioFrequency);
				Fransradio.text = string.Format("{0:0.00#}", m_RadioFrequency);

				m_mouseOld = m_mouseCurrent;
			}
			else if (m_isControllingKnob2)
			{
				if (!CheckMouseStillOnButton())
				{
					ClampOutsideButton();
					return;
				}
				Vector2 m_mouseCurrent = Input.mousePosition;

				Vector2 mouseChange = (m_mouseCurrent - m_mouseOld).normalized;
				if (m_mouseOld != Vector2.zero)
				{
					m_RadioClarity += mouseChange.x * sensitivity;
					Vector3 rotation = m_radioKnob2.transform.localEulerAngles;
					rotation.z += mouseChange.x * sensitivity;
					m_radioKnob2.transform.localRotation = Quaternion.Euler(rotation);
				}

				m_RadioClarity = Mathf.Clamp(m_RadioClarity, 0.0f, 1.0f);
				m_RadioKnob.setParameterByName("Clarity", m_RadioClarity);
				m_radioClarity.text = string.Format("{0:0.00#}", m_RadioClarity);

				m_mouseOld = m_mouseCurrent;
			}
		}
	}

	private void Start()
	{
		//m_radioCam = GetComponentInChildren<CinemachineVirtualCamera>();

		

		m_RadioKnob = RuntimeManager.CreateInstance(m_RadioKnobEvent);
		RuntimeManager.AttachInstanceToGameObject(m_RadioKnob, GetComponent<Transform>(), GetComponent<Rigidbody>());
		m_RadioKnob.start();
		m_RadioKnob.setParameterByName("Radio_Frequency", 0.81f);

		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange += ExamineRadio;
			InputHandler.Instance.PickupStateChange += CheckForRadioButton;
			InputHandler.Instance.ReleaseStateChange += CheckForRadioButtonRelease;
		}
	}

    private void OnDestroy()
    {
        
            m_RadioKnob.stop(FMOD.Studio.STOP_MODE.IMMEDIATE); //error here
            //m_RadioKnob.release(); // -- or here, when previous line is commented
            //bank.unload(); // -- or here
      
    }

	public void StopRadio()
	{
		m_RadioKnob.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
		this.enabled = false;
	}

	private void OnEnable()
	{
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange += ExamineRadio;
			InputHandler.Instance.PickupStateChange += CheckForRadioButton;
			InputHandler.Instance.ReleaseStateChange += CheckForRadioButtonRelease;
		}
		m_fpsController = m_player.GetComponent<FirstPersonController>();
		m_virtualCam = m_playerCam.GetComponent<CinemachineVirtualCamera>();
		m_collider = GetComponent<BoxCollider>();
	}

	private void OnDisable()
	{
		if (InputHandler.Instance != null)
		{
			InputHandler.Instance.PickupStateChange -= ExamineRadio;
			InputHandler.Instance.PickupStateChange -= CheckForRadioButton;
			InputHandler.Instance.ReleaseStateChange -= CheckForRadioButtonRelease;
		}
	}

	private void ExamineRadio()
	{
		if (m_interactionActive)
		{
			return;
		}

		RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
		//Ray cameraRay = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
		//Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (hit.transform != null)//Physics.SphereCast(cameraRay, 0.2f, out hit, 3.0f))
		{
			if (hit.transform.CompareTag("Radio"))
			{
				m_RadioPrompt.SetActive(true);

				m_interactionActive = true;
				m_RadioFrequency = m_lastRadioFrequency;
				m_RadioClarity = m_lastRadioClarity;
				m_radioCam.gameObject.SetActive(true);

				m_fpsController.enabled = false;
				m_virtualCam.enabled = false;
				m_collider.enabled = false;

				MouseController.m_MouseInstance.LockUnlockCursor();
				MouseController.m_MouseInstance.DeactivateCrosshair(this);

				if (m_knobText1 != null)
				{
					m_knobText1.SetActive(true);
				}
				if (m_knobText2 != null)
				{
					m_knobText2.SetActive(true);
				}
			}
		}
	}

	void CheckForRadioButton()
	{
		if (m_isControllingKnob1 || m_isControllingKnob2)
			return;
		if (!m_interactionActive)
			return;
		RaycastHit hit;
		Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(cameraRay, out hit, 3.0f))
		{
			if (hit.transform.name.Contains("button1"))
			{
				m_isControllingKnob1 = true;
				m_mouseOld = Vector2.zero;
			}
			else if (hit.transform.name.Contains("button2"))
			{
				m_isControllingKnob2 = true;
				m_mouseOld = Vector2.zero;
			}
		}
	}

	void CheckForRadioButtonRelease()
	{
		if (m_interactionActive && m_isControllingKnob1)
		{
			m_lastRadioFrequency = m_RadioFrequency;
			m_isControllingKnob1 = false;
			m_mouseOld = Vector2.zero;
		}
		else if (m_interactionActive && m_isControllingKnob2)
		{
			m_lastRadioClarity = m_RadioClarity;
			m_isControllingKnob2 = false;
			m_mouseOld = Vector2.zero;
		}
	}

	public void DeActivateRadioInteraction()
	{
		m_interactionActive = false;

		m_collider.enabled = true;
		m_fpsController.enabled = true;
		m_virtualCam.enabled = true;

		m_radioCam.gameObject.SetActive(false);

		MouseController.m_MouseInstance.ActivateCrosshair(this);

		if (m_knobText1 != null)
			m_knobText1.SetActive(false);
		if (m_knobText2 != null)
			m_knobText2.SetActive(false);
	}

	void CheckForPreciseFrequency()
	{
		if (!m_advancedPhase && m_RadioFrequency >= m_minFreq && m_RadioFrequency <= m_maxFreq)
		{
			m_freqCheckCounter += Time.deltaTime;
			if (m_freqCheckCounter >= frequencyCheckDuration)
			{
				ItemProgressionHandler.Instance.AdvancePhase();
				m_advancedPhase = true;
				if (m_flowchart != null)
					m_flowchart.SendFungusMessage("Frequency");
			}
		}
		else if (!m_advancedPhase && (m_RadioFrequency < m_minFreq || m_RadioFrequency > m_maxFreq))
		{
			m_freqCheckCounter = 0f;
		}
	}

	//Check if player is within "button-field" when moving the mouse
	bool CheckMouseStillOnButton()
	{
		RaycastHit hit;
		Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(cameraRay, out hit, 1.0f))
		{
			if (m_isControllingKnob1)
				return hit.transform.name.Equals("button1");
			else if (m_isControllingKnob2)
				return hit.transform.name.Equals("button2");
		}
		return false;
	}

	void ClampOutsideButton()
	{
		RaycastHit hit;
		Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(camRay, out hit))
		{
			if (m_isControllingKnob1)
			{
				Vector3 mouseDir = (hit.point - m_radioKnob1.transform.position).normalized;
				mouseDir = Vector3.Project(mouseDir, m_radioKnob1.transform.right);
				if (Vector3.Dot(mouseDir, m_radioKnob1.transform.right) > 0f)
				{
					m_RadioFrequency = 0.0f;
					m_RadioFrequency = Mathf.Clamp(m_RadioFrequency, 0.0f, 1.0f);
					m_RadioKnob.setParameterByName("Radio_Frequency", m_RadioFrequency);
					Fransradio.text = string.Format("{0:0.00#}", m_RadioFrequency);
				}
				else if (Vector3.Dot(mouseDir, m_radioKnob1.transform.right) < 0f)
				{
					m_RadioFrequency = 1.0f;
					m_RadioFrequency = Mathf.Clamp(m_RadioFrequency, 0.0f, 1.0f);
					m_RadioKnob.setParameterByName("Radio_Frequency", m_RadioFrequency);
					Fransradio.text = string.Format("{0:0.00#}", m_RadioFrequency);
				}
			}
			else if (m_isControllingKnob2)
			{
				Vector3 mouseDir = (hit.point - m_radioKnob2.transform.position).normalized;
				mouseDir = Vector3.Project(mouseDir, m_radioKnob2.transform.right);
				if (Vector3.Dot(mouseDir, m_radioKnob2.transform.right) > 0f)
				{
					m_RadioClarity = 0.0f;
					m_RadioClarity = Mathf.Clamp(m_RadioClarity, 0.0f, 1.0f);
					m_RadioKnob.setParameterByName("Clarity", m_RadioClarity);
					m_radioClarity.text = string.Format("{0:0.00#}", m_RadioClarity);
				}
				else if (Vector3.Dot(mouseDir, m_radioKnob2.transform.right) < 0f)
				{
					m_RadioClarity = 1.0f;
					m_RadioClarity = Mathf.Clamp(m_RadioClarity, 0.0f, 1.0f);
					m_RadioKnob.setParameterByName("Clarity", m_RadioClarity);
					m_radioClarity.text = string.Format("{0:0.00#}", m_RadioClarity);
				}
			}
		}
	}
}