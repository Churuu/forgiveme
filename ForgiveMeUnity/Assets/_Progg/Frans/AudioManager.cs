﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio;
using FMODUnity;

//tanken med scriptet är att alla ljud som ska spelas när något händer i spelet (iprincip alla) 
//ska finnas med detta script som en deklaration för vilka ljud som ska va med.

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    [EventRef]
    public string m_Pickup;

    [Header("FootstepSounds")]
    [EventRef]
    public string m_FootstepWood;
    [EventRef]
    public string m_FootstepKitchenTiles;
    [EventRef]
    public string m_FootstepCarpet;

    [EventRef]
    public string m_Crouch;

    [EventRef]
    public string m_LightBlackout;

    [EventRef]
    public string m_ThunderBolt;

    [EventRef]
    public string m_PickupFlashlight;

    [EventRef]
    public string m_PickupMirror;

    [EventRef]
    public string m_PickupNote;

    [EventRef]
    public string m_PickupLighter;

    [EventRef]
    public string m_PickupKey;

    [EventRef]
    public string m_LaurasCase;

    [EventRef]
    public string m_PickupVase;

    [EventRef]
    public string m_AlarmOff;

    //Detta är för pickup åt de olika objekt som kan plockas upp.
    public enum PickupType { Flashligt, Note, Mirror, Lighter, Key, Vase, GenericPickup };

    //Detta är för fotstegen.
    [Tooltip("The time it takes until another footstep oneshot is triggered")]
    [SerializeField] private float m_footstepDelay;
    private float m_nextFootstep = 0f;

    void Start()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    public void pickupTypeDetermine(PickupType type)
    {
        if (type == PickupType.Flashligt)
            RuntimeManager.PlayOneShot(m_PickupFlashlight, transform.position);

        else if (type == PickupType.Note)
            RuntimeManager.PlayOneShot(m_PickupNote, transform.position);

        else if (type == PickupType.Mirror)
            RuntimeManager.PlayOneShot(m_PickupMirror, transform.position);

        else if (type == PickupType.Lighter)
            RuntimeManager.PlayOneShot(m_PickupLighter, transform.position);

        else if (type == PickupType.Key)
            RuntimeManager.PlayOneShot(m_PickupKey, transform.position);

        else if (type == PickupType.Vase)
            RuntimeManager.PlayOneShot(m_PickupVase, transform.position);

        else if (type == PickupType.GenericPickup)
            RuntimeManager.PlayOneShot(m_Pickup, transform.position);

    }

    public void PlayerFootstep(int layer)
    {
        if (Time.time >= m_nextFootstep)
        {

            switch (layer)
            {
                //Wood
                case 10:
                    RuntimeManager.PlayOneShot(m_FootstepWood, transform.position);
                    break;
                //Floor tiles
                case 11:
                    RuntimeManager.PlayOneShot(m_FootstepKitchenTiles, transform.position);
                    break;
                //Carpet
                case 12:
                    RuntimeManager.PlayOneShot(m_FootstepCarpet, transform.position);
                    break;
                case 0:
                    RuntimeManager.PlayOneShot(m_FootstepWood, transform.position);
                    break;
            }
            m_nextFootstep = Time.time + m_footstepDelay;
        }
    }

    public void AlarmOff(Vector3 position)
    {
        RuntimeManager.PlayOneShot(m_AlarmOff, position);
    }

    public void PickupItemNote()
    {

    }



    public void TablePush()
    {

    }

    public void Crouch()
    {

    }

    public void ItemUse()
    {

    }

    public void Vase()
    {

    }

    public void Mirror()
    {

    }

    public void Case(Vector3 position)
    {
        RuntimeManager.PlayOneShot(m_LaurasCase, position);
    }

    public void ThunderBolt(Vector3 position)
    {
        RuntimeManager.PlayOneShot(m_ThunderBolt, position);
    }


}
