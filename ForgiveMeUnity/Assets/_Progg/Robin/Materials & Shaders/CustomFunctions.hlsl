//UNITY_SHADER_NO_UPGRADE

#ifndef CUSTOMS_INCLUDED
#define CUSTOMS_INCLUDED

void XYCoordinates_float(float2 gv, float t, float n, float w, out float x, out float y)
{
	t += n * 6.2831853071795865;
	x = ((n - .5) * 0.8);
	x += (.4 - abs(x)) * sin(3 * w) * pow(sin(w), 6) * .45;
	y = (-sin(t + sin(t + sin(t) * .5)) * .45);
	y -= (gv.x - x) * (gv.x - x);

}

void FogTrailXY_float(float trail, float2 gv, float2 dropPos, float y, out float trailOut, out float fogTrail)
{
	fogTrail = smoothstep(-0.05, 0.05, dropPos.y);
	fogTrail *= smoothstep(0.5, y, gv.y); 
	trailOut = trail*fogTrail;
	fogTrail *= smoothstep(0.05, 0.04, abs(dropPos.x));
}

void OutColor_float(float drop, float2 dropPos, float trail, float2 trailPos, sampler2D mainTex, float2 UV, float distortion, out float4 col)
{
	float2 offs = drop * dropPos + trail * trailPos;
	col = tex2D(mainTex, UV + offs * distortion);
}

void random_float(float2 p, out float pOut)
{
	p = frac(p * float2(123.34, 345.45));
	p += dot(p, p + 34.345);
	pOut = frac(p.x * p.y);
}

#endif