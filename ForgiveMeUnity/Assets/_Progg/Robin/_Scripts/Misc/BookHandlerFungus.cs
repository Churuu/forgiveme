﻿using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CommandInfo("BookSystem", "Open book", "This command opens the book")]
public class BookHandlerFungus : Command
{
    public override void OnEnter()
    {
		print("Fett");
        FindObjectOfType<BookScrolling>().OpenCloseBook(true);
		Continue();
    }

}
