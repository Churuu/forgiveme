﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CutScene : MonoBehaviour
{

    GameObject fadeImage;
    public void OnEnter()
    {
        fadeImage = GameObject.Find("FadeImage");
        LeanTween.alpha(fadeImage.GetComponent<RectTransform>(), 1, 0.01f);
        Invoke("OnExit", 10);
    }

    public void OnExit()
    {
        MusicHandler.Instance.StopSymbolmusic();
        LeanTween.alpha(fadeImage.GetComponent<RectTransform>(), 0, 0.01f);
        if (FindObjectOfType<ExamineSymbols>().m_GoodEnding)
            SceneManager.LoadScene("GoodEnding");
        else
            SceneManager.LoadScene("BadEnding");
    }
}
