﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutroScene : MonoBehaviour
{
    Animator m_AnimGood, m_AnimBad;
    [SerializeField]
    GameObject m_LauraGood, m_LauraBad, m_vCam, m_vHead;


    public void Start()
    {
        m_LauraGood.SetActive(false);
        m_vCam.SetActive(false);
        m_AnimGood = m_LauraGood.GetComponent<Animator>();
        m_AnimBad = m_LauraBad.GetComponent<Animator>();
    }

    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (FindObjectOfType<ExamineSymbols>().PlayerHasFinished())
        {
            if (FindObjectOfType<ExamineSymbols>().m_GoodEnding)
                m_LauraGood.SetActive(true);
            else
                m_LauraBad.SetActive(true);

            FindObjectOfType<Flashlight>().ToggleFlashlight();
            MouseController.m_MouseInstance.LockUnlockCursor();
            MouseController.m_MouseInstance.DeactivateCrosshair(this);
            m_vHead.SetActive(false);
            m_vCam.SetActive(true);
            m_AnimBad.SetTrigger("OutroScene");
            m_AnimGood.SetTrigger("OutroScene");
        }
    }
}
