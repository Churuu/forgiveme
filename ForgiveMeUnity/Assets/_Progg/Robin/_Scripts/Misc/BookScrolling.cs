﻿using UnityEngine;
using UnityEngine.UI;
using Fungus;


/// <summary>
/// A script to allow the player to look trough a book and find the correct page
/// </summary>

public class BookScrolling : MonoBehaviour
{
    [Header("Display image")]
    [SerializeField]
    private Image m_Image;

    [SerializeField]
    GameObject m_BookHandler;

    [SerializeField]
    Flowchart flowchart;

    [SerializeField]
    InputField m_InputField;

    void Start()
    {
        m_InputField = GetComponent<InputField>();
    }

    public void OpenCloseBook(bool state)
    {
        m_BookHandler.SetActive(state);
    }

    public void CheckBookNumber()
    {
        if (m_InputField.text == "84")
        {
            m_Image.gameObject.SetActive(true);
            ItemProgressionHandler.Instance.AdvancePhase();
            m_InputField.gameObject.SetActive(false);
        }
        else
        {
            flowchart.SendFungusMessage("WrongPage");
        }
    }
}
