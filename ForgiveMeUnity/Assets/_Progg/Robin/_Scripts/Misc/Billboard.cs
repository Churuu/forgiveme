﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        //Looks at the camera with the main tag
        transform.LookAt(Camera.main.transform.position, Vector3.up);            
    }
}
