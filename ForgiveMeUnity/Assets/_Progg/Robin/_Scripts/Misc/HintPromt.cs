﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintPromt : MonoBehaviour
{
    [SerializeField]
    [Header("UI Text")] [Tooltip("Skapa en UI text i en canvas som täcker skärmen och lägg till den i fältet nedan")]
    private Text m_TextField;

    private void OnTriggerEnter(Collider col)
    {
        if(m_TextField == null)
        {
            Debug.LogError("Ingen UI text hittades, skapa en UI text och lägg till den i inspektorn!");
            return;
        }

        if(ItemProgressionHandler.Instance.GetCurrentPhase() == ItemProgressionHandler.Phase.first)
        LeanTween.alphaText(m_TextField.GetComponent<RectTransform>(), 1f, 1f);

    }

    private void OnTriggerExit(Collider col)
    {
        if (m_TextField == null)
        {
            Debug.LogError("Ingen UI text hittades, skapa en UI text och lägg till den i inspektorn!");
            return;
        }

        LeanTween.alphaText(m_TextField.GetComponent<RectTransform>(), 0f, 1f);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, transform.localScale);
    }
}
