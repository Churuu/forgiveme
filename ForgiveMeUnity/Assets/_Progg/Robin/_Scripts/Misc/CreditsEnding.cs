﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class CreditsEnding : MonoBehaviour
{
    VideoPlayer m_VideoPlayer;

    // Start is called before the first frame update
    void Start()
    {
        m_VideoPlayer = GetComponent<VideoPlayer>();
        float clipTime = Convert.ToSingle(m_VideoPlayer.clip.length);
        Invoke("SwitchToMainMenuOnFinish", clipTime);
    }

    void SwitchToMainMenuOnFinish()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
