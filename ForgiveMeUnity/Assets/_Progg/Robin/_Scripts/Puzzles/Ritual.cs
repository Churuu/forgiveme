﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

public class Ritual : MonoBehaviour
{

    [SerializeField]
    private Text m_RitualText;

    [SerializeField]
    private GameObject[] m_RitualItems;

    [SerializeField]
    private GameObject[] m_candleLights;

    [SerializeField]
    bool m_AdvancePhaseOnItemPlacement = true, m_AdvancePhaseOnBeginRitual = true, m_AdvancePhaseOnRitualFinish;

    InputHandler m_InputHandler;

    [SerializeField]
    MoveInteractObject m_Book;

    [SerializeField] GameObject m_BookNote;

    string m_CurrentDisplayAction = "Place ritual items";

    int j = 4;

    Action m_CurrentAction;

    bool m_DisplayTextEnabled = false;

    #region events
    private void Awake()
    {
        m_InputHandler = FindObjectOfType<InputHandler>();
    }

    private void Start()
    {
        m_CurrentAction = PlaceItems;
    }

    private void OnEnable()
    {
        m_InputHandler.PickupStateChange += Interact;
    }

    private void OnDisable()
    {
        m_InputHandler.PickupStateChange -= Interact;
    }
    #endregion

    #region Frame Updates

    private void Update()
    {
        DisplayAction();
    }
    #endregion

    #region RitualHandler
    //Display correct action for the ritual
    private void DisplayAction()
    {
        RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
        if (hit.collider != null && hit.transform.CompareTag("Ritual"))
        {
            if (PlayerHasRitualItems() || PlayerHasExcessRitualItems() || m_DisplayTextEnabled)
                m_RitualText.text = m_CurrentDisplayAction;
            else
                m_RitualText.text = "";
        }
        else
            m_RitualText.text = "";

    }

    //Actions to control if the player should place items or start the ritual
    private void Interact()
    {
        RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
        if (hit.collider != null && hit.collider.CompareTag("Ritual"))
        {
            m_CurrentAction();
        }
    }

    //Places ritual items in the correct location
    public void PlaceItems()
    {
        if (PlayerHasRitualItems())
        {
            for (int i = 0; i < 4; i++)
            {
                m_RitualItems[i].SetActive(true);
            }

            if (m_candleLights.Length > 0)
            {
                m_CurrentDisplayAction = "Light Candles";
                m_DisplayTextEnabled = true;
                m_CurrentAction = LightCandles;
            }
            else
            {
                m_CurrentDisplayAction = "Begin Ritual";
                m_DisplayTextEnabled = true;
                m_CurrentAction = BeginRitualCutscene;
            }
            InventoryHandler.RemoveItemFromInventory("Candle1");
            InventoryHandler.RemoveItemFromInventory("Candle2");
            InventoryHandler.RemoveItemFromInventory("Candle3");
            InventoryHandler.RemoveItemFromInventory("Book");
            MusicHandler.Instance.ritualmusic();
        }
    }


    public void LightCandles()
    {
        if (InventoryHandler.CompareName("Lighter"))
        {
            for (int i = 0; i < m_candleLights.Length; i++)
            {
                m_candleLights[i].SetActive(true);
            }

            m_CurrentDisplayAction = "Begin Ritual";
            m_CurrentAction = BeginRitualCutscene;

        }
    }

    public void BeginRitualCutscene()
    {
        m_CurrentDisplayAction = "Place " + m_RitualItems[j].name;
        m_DisplayTextEnabled = false;
        if (m_AdvancePhaseOnBeginRitual)
            ItemProgressionHandler.Instance.AdvancePhase();
        m_Book.Move();
        m_BookNote.SetActive(true);
        Flowchart.BroadcastFungusMessage("Ritual");

        m_CurrentAction = PlaceExcessItems;
    }

    private void PlaceExcessItems()
    {
        GameObject nextItem = m_RitualItems[j];
        if (InventoryHandler.CompareName(nextItem.name))
        {
            nextItem.SetActive(true);
            InventoryHandler.RemoveItemFromInventory(nextItem);
            if (j < m_RitualItems.Length - 1)
            {
                j++;
                m_CurrentDisplayAction = "Place " + m_RitualItems[j].name;

                if (m_AdvancePhaseOnItemPlacement)
                    ItemProgressionHandler.Instance.AdvancePhase();
            }
            else
            {
                if (m_AdvancePhaseOnRitualFinish)
                    ItemProgressionHandler.Instance.AdvancePhase();
                Flowchart.BroadcastFungusMessage("Artifacts");
                m_CurrentDisplayAction = "";
                m_CurrentAction = null;
            }
        }
    }

    //Advance to next phase, play radio sound in here and play animation
    public void StartRadio()
    {
        FindObjectOfType<RadioManager>().enabled = true;
        MusicHandler.Instance.RitualMusic.DestroyRitualMusic();

    }
    #endregion

    //Check if the player has the correct items to start the ritual
    public bool PlayerHasRitualItems()
    {
        if (InventoryHandler.CompareName("Candle1")
            && InventoryHandler.CompareName("Candle2")
            && InventoryHandler.CompareName("Candle3")
            && InventoryHandler.CompareName("Lighter")
            && InventoryHandler.CompareName("Book") && m_CurrentAction == PlaceItems)
            return true;
        else
            return false;
    }
    public bool PlayerHasExcessRitualItems()
    {
        if (InventoryHandler.CompareName(m_RitualItems[j].name))
            return true;
        else
            return false;
    }
}
