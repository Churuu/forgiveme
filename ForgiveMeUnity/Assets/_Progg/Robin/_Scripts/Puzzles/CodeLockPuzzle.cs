﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;


public class CodeLockPuzzle : MonoBehaviour
{
    public StudioEventEmitter laurasstinger;

    public int[] m_CorrectCode = new int[3];
    int[] m_CodeNumbers = new int[3];

    [SerializeField]
    Text m_CodeText;
    [SerializeField]
    Text m_DisplayText;

    [SerializeField]
    GameObject m_CodeLockPuzzlePromt,
        m_LaurasNotebook, 
        m_CodeLockFlowchart;

    [SerializeField] [EventRef] private string m_drawerMoveAudio;

    GameObject m_CodeLockDrawer;

    static bool m_Unlocked = false;

    RaycastHit hit;

    InputHandler m_InputHandler;

    public bool CompareNumbers(int[] vsNumbers)
    {
        for (int i = 0; i < m_CodeNumbers.Length; i++)
        {
            if (GetNumber(i) == vsNumbers[i])
                continue;
            else return false;


        }
        return true;
    }

    public void EnableCodeLock()
    {
        hit = FirstPersonController.Instance.GetRaycastHit();
        if (hit.collider != null && hit.collider.CompareTag("CodeLock") && !m_CodeLockPuzzlePromt.activeSelf && !m_Unlocked)
        {
            m_CodeLockDrawer = hit.collider.gameObject;
            m_CodeLockPuzzlePromt.SetActive(true);
            MouseController.m_MouseInstance.LockUnlockCursor();
        }
    }

    public static bool GetUnlockedState()
    {
        return m_Unlocked;
    }

    public static void SetUnlockState(bool state)
    {
        m_Unlocked = state;
    }

    public void DisableCodeLock()
    {
        m_CodeLockPuzzlePromt.SetActive(false);
        MouseController.m_MouseInstance.LockUnlockCursor();

        if (CompareNumbers(m_CorrectCode))
        {
            m_CodeLockDrawer.layer = 9;

            if (!string.IsNullOrEmpty(m_drawerMoveAudio))
			    RuntimeManager.PlayOneShot(m_drawerMoveAudio, transform.position);

			LeanTween.moveLocalZ(m_CodeLockDrawer, 45f, 0.5f).setOnComplete(EnableCollision);
            SetUnlockState(true);
            InventoryHandler.StoreInventoryItem(m_LaurasNotebook);
            m_CodeLockFlowchart.SetActive(true);
            laurasstinger.Play();
        }
    }

    void EnableCollision()
    {
        m_CodeLockDrawer.layer = 0;
    }

    public void AddNumber(int slot)
    {
        if (GetNumber(slot) < 9)
            m_CodeNumbers[slot]++;
        else if (GetNumber(slot) == 9)
            m_CodeNumbers[slot] = 0;

        UpdateUI();
    }

    public void SubtractNumbner(int slot)
    {
        if (GetNumber(slot) > 0)
            m_CodeNumbers[slot]--;
        else if (GetNumber(slot) == 0)
            m_CodeNumbers[slot] = 9;

        UpdateUI();
    }

    public int GetNumber(int slot)
    {
        return m_CodeNumbers[slot];
    }

    private void UpdateUI()
    {
        m_CodeText.text = GetNumber(0).ToString() + "                 " + GetNumber(1).ToString() + "                 " + GetNumber(2).ToString();
    }

    private void Awake()
    {
        m_InputHandler = FindObjectOfType<InputHandler>();
    }

    private void Update()
    {
        hit = FirstPersonController.Instance.GetRaycastHit();
        if (hit.collider != null && hit.collider.CompareTag("CodeLock") && !m_CodeLockPuzzlePromt.activeSelf && !m_Unlocked)
            m_DisplayText.text = "Unlock";
        else 
            m_DisplayText.text = "";
    }

    private void OnEnable()
    {
        m_InputHandler.PickupStateChange += EnableCodeLock;
    }

    private void OnDisable()
    {
        m_InputHandler.PickupStateChange -= EnableCodeLock;
    }

}
