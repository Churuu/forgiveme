﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ExamineSymbols : MonoBehaviour
{
    #region variables
    [SerializeField]
    private float m_CastRadius;
    [SerializeField]
    private float m_MaxCastDistance;

    [Tooltip("Drag the symbols here that should turn on when you pick up a symbol around the house")]
    [SerializeField]
    private GameObject[] m_SymbolsOnCanvas;

    [SerializeField]
    private GameObject[] m_Symbols;

    [SerializeField]
    MoveInteractObject m_DoorObjectToOpen;

    [HideInInspector]
    static bool m_SymbolsActivated = false;

    List<GameObject> m_SymbolOrderPickup = new List<GameObject>();

    float intensity = 0f;
    public bool m_GoodEnding = false;

    #endregion

    #region 
    private void OnEnable()
    {
        if (InputHandler.Instance != null)
            InputHandler.Instance.PickupStateChange += ExamineSymbol;
    }

    private void OnDisable()
    {
        if (InputHandler.Instance != null)
            InputHandler.Instance.PickupStateChange -= ExamineSymbol;
    }

    private void Start()
    {
        InputHandler.Instance.PickupStateChange += ExamineSymbol;

        for (int i = 0; i < m_SymbolsOnCanvas.Length; i++)
        {
            m_Symbols[i].SetActive(false);
            m_SymbolsOnCanvas[i].SetActive(false);
        }
    }

    public void ActivatePuzzle()
    {
        m_Symbols[0].SetActive(true);
        foreach (var symbol in m_SymbolsOnCanvas)
        {
            symbol.SetActive(true);
        }
        FindObjectOfType<Flashlight>().ToggleFlashlight();
        m_DoorObjectToOpen.Move();
        m_DoorObjectToOpen.gameObject.tag = "Untagged";
        MusicHandler.Instance.StartSymbolmusic();
    }
    #endregion

    #region SymbolHandler
    //Examine symbols on the map
    private void ExamineSymbol()
    {
        //Simple spehere cast to compansate for player aim error
        RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();

        if (hit.collider != null && hit.collider.CompareTag("Symbol"))
        {
            //Activate the rest of the symbols when the first one has been picked up;
            if (!m_SymbolsActivated)
                ActivateAllSymbols();

            Symbol symbol = hit.collider.GetComponent<Symbol>();

            symbol = FindSymbol(symbol);
            LeanTween.alpha(symbol.GetComponent<RectTransform>(), 1f, 0.5f);
            //Increase music intensity
            intensity += 0.2f;
            MusicHandler.Instance.UpdateSymbolProgress(intensity);
            m_SymbolOrderPickup.Add(hit.collider.gameObject);
            hit.collider.gameObject.SetActive(false);
        }
    }

    public bool PlayerHasFinished()
    {

       if(m_SymbolOrderPickup.Count != 5)
            return false;

        m_GoodEnding = true;
        for (int i = 0; i < m_SymbolsOnCanvas.Length; i++)
        {
            if (m_SymbolsOnCanvas[i].GetComponent<Symbol>().m_SymbolType == m_SymbolOrderPickup[i].GetComponent<Symbol>().m_SymbolType)
                continue;
            else
            {
                m_GoodEnding = false;
                break;
            }
        }
        return true;
    }

    //Compare the symbol that was picked up with the one on the canvas and return the one on the canvas
    private Symbol FindSymbol(Symbol symbol)
    {
        for (int i = 0; i < m_SymbolsOnCanvas.Length; i++)
        {
            var canvasSymbol = m_SymbolsOnCanvas[i].GetComponent<Symbol>();
            var hitSymbol = symbol;
            if (canvasSymbol.m_SymbolType == hitSymbol.m_SymbolType)
            {
                return canvasSymbol;
            }
        }
        Debug.LogError("Could not find any symbols on canvas that matches the symbol picked up");
        return null;
    }


    private void ActivateAllSymbols()
    {
        foreach (var gObject in m_Symbols)
        {
            gObject.SetActive(true);
            m_SymbolsActivated = true;
        }
    }
    #endregion


    public static bool GetSymbolState()
    {
        return m_SymbolsActivated;
    }

    public static void SetSymbolState(bool state)
    {
        m_SymbolsActivated = state;
    }
}
