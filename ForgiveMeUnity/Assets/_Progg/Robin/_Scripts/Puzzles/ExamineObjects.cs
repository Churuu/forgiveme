﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// För att examinera föremål lägger du ut ett ojekt som skall examineras och ge den "Examine" tag
/// Skapa sedan en canvas om ingen redan finns och skapa ett ojekt med en textruta i, detta ojekt är det som kommer sättas på och stängas av.
/// </summary>

public class ExamineObjects : MonoBehaviour
{

    [Tooltip("How much error the player can have when looking at objects to interact with")]
    public float m_CastRadius;
    [Tooltip("How far away the player can examine items from")]
    public float m_MaxCastDistance;

    [Tooltip("Texten för promten")]
    [SerializeField]
    private Text m_ExamineText, m_DisplayText;

    [SerializeField]
    private GameObject m_ExaminePromt;


	InputHandler m_InputHandler;
    RaycastHit hit;

    private void Awake()
    {
        m_InputHandler = FindObjectOfType<InputHandler>();
    }

    private void OnEnable()
    {
        m_InputHandler.PickupStateChange += ExamineObject;
	}

    private void OnDisable()
    {
        m_InputHandler.PickupStateChange -= ExamineObject;
	}


    private void ExamineObject()
    {
        if (PlayerLookingAtExamine())
        {

            m_DisplayText.text = "";
            m_ExaminePromt.SetActive(true);

            int phaseInt = (int)ItemProgressionHandler.Instance.GetCurrentPhase();
            var description = hit.collider.GetComponent<ExamineText>();

            if (description.m_Descriptions.Length-1 >= phaseInt)
                m_ExamineText.text = description.m_Descriptions[phaseInt];
            else
                m_ExamineText.text = description.m_Descriptions[description.m_Descriptions.Length-1];



            MouseController.m_MouseInstance.LockUnlockCursor();
        }
    }

    bool PlayerLookingAtExamine()
    {
        hit = FirstPersonController.Instance.GetRaycastHit();
        if (hit.collider != null && hit.transform.CompareTag("Examine") && !m_ExaminePromt.activeSelf)
            return true;
        else return false;
    }
}
