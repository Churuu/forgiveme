﻿using System;
using UnityEngine;

[Serializable]
public enum SymbolType
{
    SymbolOne,
    SymbolTwo,
    SymbolThree,
    SymbolFour,
    SymbolFive
}
[Serializable]
public class Symbol : MonoBehaviour
{
    public SymbolType m_SymbolType;
}
