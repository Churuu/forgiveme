﻿using FMOD;
using MoonSharp.Interpreter.CoreLib;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct position
{
    public float x, y, z;
    
    public position(float X, float Y, float Z)
    {
        x = X;
        y = Y;
        z = Z;
    }
}
[Serializable]
public struct rotation
{
    public float x, y, z;

    public rotation(float X, float Y, float Z)
    {
        x = X;
        y = Y;
        z = Z;
    }
}
[Serializable]
public struct CustomGameObject
{
    public position m_Position;
    public rotation m_Rotation;

    public CustomGameObject(position position, rotation rotation)
    {
        m_Position = position;
        m_Rotation = rotation;
    }
}

[Serializable]
public struct BoolData
{
    public string name;
    public bool state;

    public BoolData(string name, bool state)
    {
        this.name = name;
        this.state = state;
    }
}
[Serializable]
public struct FloatData
{
    public string name;
    public float number;

    public FloatData(string name, float number)
    {
        this.name = name;
        this.number = number;
    }
}


[Serializable]
public class Save : MonoBehaviour
{
    public List<CustomGameObject> m_GameObjects = new List<CustomGameObject>();
    public List<string> m_InventoryItems = new List<string>();
    public ItemProgressionHandler.Phase phase;
    //public List<object> m_StoredDataTypes = new List<object>();
    public Dictionary<string, object> m_DataTypes = new Dictionary<string, object>();
    public bool m_SymbolsActivated, m_Unlocked;
}
