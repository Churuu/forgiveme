﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class SaveSystem : MonoBehaviour
{

    public GameObject[] m_GameObjects;

    [SerializeField]
    bool m_LoadSaveOnStart = false;

    Ritual m_Ritual;

    private Save SaveData()
    {
        Save save = new Save();

        for (int i = 0; i < m_GameObjects.Length; i++)
        {
            Vector3 pos = m_GameObjects[i].transform.position;
            Vector3 rotation = m_GameObjects[i].transform.eulerAngles;

            position gameObjectPosition = new position(pos.x, pos.y, pos.z);
            rotation gameObjectRotation = new rotation(rotation.x, rotation.y, rotation.z);

            save.m_GameObjects.Add(new CustomGameObject(gameObjectPosition, gameObjectRotation));
        }

        save.phase = ItemProgressionHandler.Instance.GetCurrentPhase();
        for (int i = 0; i < InventoryHandler.GetInventorySize(); i++)
        {
            var inventoryObjects = InventoryHandler.GetInventoryNameList();
            string name = inventoryObjects[i];
            save.m_InventoryItems.Add(name);
        }
        save.m_Unlocked = CodeLockPuzzle.GetUnlockedState();
        save.m_SymbolsActivated = ExamineSymbols.GetSymbolState();
        return save;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F3))
            SaveGame();
        else if (Input.GetKeyDown(KeyCode.F4))
            LoadGame();
    }

    private void Start()
    {
        if (m_LoadSaveOnStart)
            LoadGame();

        m_Ritual = FindObjectOfType<Ritual>();
    }

    public void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        Save save = SaveData();
        bf.Serialize(file, save);
        file.Close();

    }

    public void DeleteOldSave()
    {
        File.Delete(Application.persistentDataPath + "/gamesave.save");
    }

    public void LoadGame()
    {
        var save = GetSaveData();
        int i = 0;
        foreach (var obj in save.m_GameObjects)
        {
            m_GameObjects[i].transform.position = new Vector3(obj.m_Position.x, obj.m_Position.y, obj.m_Position.z);
            m_GameObjects[i].transform.eulerAngles = new Vector3(obj.m_Rotation.x, obj.m_Rotation.y, obj.m_Rotation.z);
            i++;
        }

        CodeLockPuzzle.SetUnlockState(save.m_Unlocked);
        ExamineSymbols.SetSymbolState(save.m_SymbolsActivated);
		ItemProgressionHandler.Instance.LoadToPhase(save.phase);//AdvancePhase(save.phase);

		var objectlist = FindObjectsOfType<InventoryObject>();
        foreach (var item in objectlist)
        {
            item.LoadGameObjectIntoInventory(save.m_InventoryItems);
        }

    }

    private Save GetSaveData()
    {
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            var save = (Save)bf.Deserialize(file);
            file.Close();
            return save;
        }
        return null;
    }
}
