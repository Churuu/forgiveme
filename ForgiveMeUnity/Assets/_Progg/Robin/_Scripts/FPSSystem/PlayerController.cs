﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerSettings
{
    public float m_Acceleration;
    [Space]
    public float m_MaxForceStanding;
    public float m_MaxForceCrouching;

    [Space]
    public GameObject m_CrouchCamera;
    public GameObject m_StandingCamera;

    public CapsuleCollider m_CrouchCollider;
    public CapsuleCollider m_StandingCollider;
}


public class PlayerController : MonoBehaviour
{
    [Header("Player Settings")]
    public PlayerSettings m_PlayerSettings;
    [Space]

    [SerializeField] Transform m_VirtualCamera;

    bool m_IsCrouched;

    Rigidbody m_RigidBody;



    void Start()
    {
        m_RigidBody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        UpdateMovement();
        UpdateCrouch();
    }

    void UpdateMovement()
    {
        //If The mouse is unlocked then return to update
        if (!MouseController.m_MouseInstance.IsCursorLocked())
            return;

        float moveX = Input.GetAxis("Horizontal");
        float moveY = Input.GetAxis("Vertical");


        Vector2 input = new Vector2(moveX, moveY);
        Vector3 movement = new Vector3(input.x, m_RigidBody.velocity.y, input.y).normalized * m_PlayerSettings.m_Acceleration;
        ; // Change speed if player crouched

        Vector3 velocity = m_VirtualCamera.TransformDirection(movement);
        velocity.y = m_RigidBody.velocity.y;

        if (m_RigidBody.velocity.magnitude < (m_IsCrouched ? m_PlayerSettings.m_MaxForceCrouching : m_PlayerSettings.m_MaxForceStanding))
            m_RigidBody.AddForce(velocity);
    }

    void UpdateCrouch()
    {
        if (Input.GetButton("Crouch"))
        {
            m_IsCrouched = true;

            if (m_PlayerSettings.m_StandingCamera.transform.localPosition.y == 0.5f)
                LeanTween.moveLocalY(m_PlayerSettings.m_StandingCamera, -0.25f, 0.5f).setEaseLinear();

            /*m_PlayerSettings.m_CrouchCamera.SetActive(true);
            m_PlayerSettings.m_CrouchCollider.enabled = true;

            m_PlayerSettings.m_StandingCamera.SetActive(false);
            m_PlayerSettings.m_StandingCollider.enabled = false;*/
        }
        else
        {
            if (m_PlayerSettings.m_StandingCamera.transform.localPosition.y == -0.25f)
                LeanTween.moveLocalY(m_PlayerSettings.m_StandingCamera, 0.5f, 0.5f).setEaseLinear();
            
            m_IsCrouched = false;
            /*
            m_PlayerSettings.m_CrouchCamera.SetActive(false);
            m_PlayerSettings.m_CrouchCollider.enabled = false;

            m_PlayerSettings.m_StandingCamera.SetActive(true);
            m_PlayerSettings.m_StandingCollider.enabled = true;*/
        }
    }
}
