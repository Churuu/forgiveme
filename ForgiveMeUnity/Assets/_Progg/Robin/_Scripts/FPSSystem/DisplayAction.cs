﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayAction : MonoBehaviour
{

    [SerializeField]
    Text m_DisplayText;

	private bool m_displayActive = true;

    // Update is called once per frame
    void Update()
    {
        ActivateDisplayOnRaycast();
    }


    void ActivateDisplayOnRaycast()
    {
		if (!m_displayActive)
			return;
        RaycastHit hit = FirstPersonController.Instance.GetRaycastHit();
        MouseController.m_MouseInstance.ActivateCrosshair(this);
        m_DisplayText.text = "";
        if (hit.collider != null && !hit.transform.CompareTag("Untagged"))
        {
            string tag = hit.collider.tag;
            switch (tag)
            {
                case "Symbol":
                    m_DisplayText.text = "Activate";
                    MouseController.m_MouseInstance.DeactivateCrosshair(this);
                    break;
                case "Examine":
                    m_DisplayText.text = "Examine";
                    MouseController.m_MouseInstance.DeactivateCrosshair(this);
                    break;
                case "Radio":
					m_DisplayText.text = "Investigate";
					MouseController.m_MouseInstance.DeactivateCrosshair(this);
					break;
				case "Vase":
					m_DisplayText.text = "Search";
					MouseController.m_MouseInstance.DeactivateCrosshair(this);
					break;
                case "Teabag":
					m_DisplayText.text = "Examine";
					MouseController.m_MouseInstance.DeactivateCrosshair(this);
					break;
                case "PickUp":
					m_DisplayText.text = "Pick up";
					MouseController.m_MouseInstance.DeactivateCrosshair(this);
					break;
                case "Move":
					if (!hit.transform.GetComponent<MoveInteractObject>().GetCanMove())
						break;
					m_DisplayText.text = "Move";
					MouseController.m_MouseInstance.DeactivateCrosshair(this);
					break;
                case "Skrin":
					m_DisplayText.text = "Open";
					MouseController.m_MouseInstance.DeactivateCrosshair(this);
					break;
				case "Key":
					m_DisplayText.text = "Pick up";
					MouseController.m_MouseInstance.DeactivateCrosshair(this);
					break;
				case "Alarm":
					m_DisplayText.text = "Shut off";
					MouseController.m_MouseInstance.DeactivateCrosshair(this);
					break;
			}
        }
    }

	public void SetDisplayActiveState(bool state)
	{
		m_displayActive = state;
		if (state == false)
			m_DisplayText.text = string.Empty;
	}

	public bool GetDisplayActiveState()
	{
		return m_displayActive;
	}
}
