﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestController : MonoBehaviour
{

    Rigidbody rb;
    [SerializeField] Transform m_MainCamera;
    [SerializeField] float m_Speed;

    float mouseY = 0;
    float mouseX = 0;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        mouseX += Input.GetAxisRaw("Mouse Y");
        mouseY += Input.GetAxisRaw("Mouse X");

        m_MainCamera.rotation = Quaternion.Euler(new Vector3(-mouseX, mouseY, 0));

        Vector3 movement = new Vector3(x, 0, z);
        movement = m_MainCamera.TransformDirection(movement);

        rb.velocity = movement * m_Speed * Time.deltaTime;
    }
}
