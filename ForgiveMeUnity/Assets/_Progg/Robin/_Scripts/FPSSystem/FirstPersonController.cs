﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;
using UnityEditor;

/// <summary>
/// Handles moving the player, looking around, crouching and other first person controller aspects.
/// </summary>

[Obsolete]
[RequireComponent(typeof(CharacterController))]
public class FirstPersonController : MonoBehaviour
{
    #region variables
    [SerializeField]
    private float m_Speed = 350, m_Gravity = 9.81f, m_CastRadius, m_MaxCastDistance;


    private bool m_IsCrouching = false, m_LockCamera = false;

    private CharacterController m_CharController;

    [Header("Height properties")]
    [SerializeField]
    float m_StandardHeight = 1.45f;
    [SerializeField]
    float m_StandardCameraHeigh = 0.5f;

    float m_CrouchHeight = 0;
    float m_CameraCrouchHeight = 0;


    private Vector2 m_GravityVelocity = Vector2.zero;

    public static FirstPersonController Instance;

    InputHandler m_InputHandler;


    [SerializeField]
    [Header("Brain")]
    private Transform m_Camera;

    [SerializeField]
    [Header("Head")]
    private CinemachineVirtualCamera m_VirtualCamera;
    #endregion

    #region Start
    // Start is called before the first frame update
    void Start()
    {
        m_CharController = GetComponent<CharacterController>();
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
        m_InputHandler = FindObjectOfType<InputHandler>();
    }

    //Enable and disable events
    private void OnEnable()
    {
        m_InputHandler.CrouchStateChange += ToggleCrouch;
    }

    private void OnDisable()
    {
        m_InputHandler.CrouchStateChange -= ToggleCrouch;
    }
    #endregion

    #region Control
    // Update is called once per frame
    void Update()
    {
        Movement();
        Crouch();
    }

    private void Movement()
    {
        //Fetch input from inputhandler and create vector3
        Vector2 movement2D = InputHandler.Instance.GetXYMove();

        Vector3 movement = new Vector3(-movement2D.y, 0f, movement2D.x);

        //Normalize values and tie to frames
        movement = movement.normalized;
        movement *= m_Speed;
        movement = m_Camera.TransformDirection(movement);
        movement.y = 0;

        //Move in direction of camera
        m_CharController.Move(movement * Time.deltaTime);



        //Cast a ray down to check if the player is standing on a surface and then add gravity if hit is null
        RaycastHit hit;
        Physics.Raycast(transform.position, -transform.up, out hit, m_CharController.height * 0.6f);




        if (hit.collider == null)
        {
            m_GravityVelocity.y -= 9.81f * Time.deltaTime;
        }
        else
        {
            m_GravityVelocity.y = 0;

            int layer = hit.collider.gameObject.layer;
            if (m_CharController.velocity.magnitude > 0 && AudioManager.Instance != null)
                AudioManager.Instance.PlayerFootstep(layer);
        }

        m_CharController.Move(m_GravityVelocity * Time.deltaTime);
    }

    //Controls if the player should crouch or not.
    private void Crouch()
    {
        var virtualCamera = m_VirtualCamera.GetCinemachineComponent<CinemachineTransposer>();
        switch (m_IsCrouching)
        {

            //Player is pressing the crouch button and should crouch,i use a leantween to make the process smother
            case true:
                //LeanTween.value(virtualCamera.m_FollowOffset.y, m_CameraCrouchHeight, 0.3f).setOnUpdate((float val) => { virtualCamera.m_FollowOffset.y = val; });
                m_CharController.height = m_CrouchHeight;
                //m_CharController.center = new Vector3(0, -0.34f, 0);
                //LeanTween.value(m_CharController.height, m_CrouchHeight, 0.5f).setEase(m_EasingType).setOnUpdate((float val) => { m_CharController.height = val; });

                //Moves the camera to crouch position more smoothly

                break;

            case false:
                //LeanTween.value(virtualCamera.m_FollowOffset.y, m_StandardCameraHeigh, 0.3f).setOnUpdate((float val) => { virtualCamera.m_FollowOffset.y = val; });
                m_CharController.height = m_StandardHeight;
                //m_CharController.center = new Vector3(0, 0.14f, 0);
                //LeanTween.value(m_CharController.height, m_StandardHeight, 0.5f).setEase(m_EasingType).setOnUpdate((float val) => { m_CharController.height = val; });

                //Moves the camera back to standard position more smoothly

                break;
        }
        m_IsCrouching = false;

    }
    #endregion

    #region 

    void ToggleCrouch()
    {
        //Changes the current crouch state
        m_IsCrouching = true;
    }

    public RaycastHit GetRaycastHit()
    {
        if (MouseController.m_MouseInstance.IsCursorLocked())
        {
            RaycastHit hit;
            Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.SphereCast(cameraRay, m_CastRadius, out hit, m_MaxCastDistance);
            return hit;
        }
        else return new RaycastHit();

    }

    #endregion

}
