﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;
using UnityEngine.UI;

[Serializable]
public struct MouseSettings
{
    public float horizontal;
    public float vertical;

    public float maxClampX, minClampX;

    public GameObject crouchingCam, standingCam;
}

public class MouseController : MonoBehaviour
{



    [SerializeField] [Header("Mouse Settings")]
    MouseSettings m_Settings;



    private bool m_CursorVisible = false;
    private bool m_CursorLocked = true;
	private bool m_crosshairTaken, m_crosshairReleased, m_inventoryToggle, m_pauseMenuToggle,
        m_crosshairOverride;

	public static MouseController m_MouseInstance;

    CinemachinePOV m_VirtualCam;

	[SerializeField]
	private GameObject m_crosshair;

	private MonoBehaviour m_lastOverrider;
	private GameObject m_inventoryUIObject;
	private CanvasGroup m_pauseMenu;

    float m_CamX;
    float m_CamY;


	private void Awake()
    {
        if (m_MouseInstance != null)
            Destroy(this);
        else
            m_MouseInstance = this;

		//m_inventoryUIObject = FindObjectOfType<InventoryScript>().transform.GetChild(0).gameObject;
		if (GameObject.Find("PauseMenu"))
		    m_pauseMenu = GameObject.Find("PauseMenu").GetComponent<CanvasGroup>();
	}

    private void Start()
    {
        /*if (m_Body == null)
            m_Body = GameObject.Find("Body");

        if (m_Head == null)
            m_Head = GameObject.Find("Head");

        Cursor.visible = m_CursorVisible;
        m_VirtualCam = m_Head.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachinePOV>();

        m_VirtualCam.m_HorizontalAxis.m_MaxSpeed = m_MouseSensitivity.horizontal;
        m_VirtualCam.m_VerticalAxis.m_MaxSpeed = m_MouseSensitivity.vertical;*/
    }

    // Update is called once per frame
    void Update()
    {
        CursorLockState();
        //UpdateCrosshair();
        UpdateCameraRotation();
	}

    void UpdateCameraRotation()
    {
        //If cursor is locked return to update
        if (!IsCursorLocked())
            return;

        m_CamX += Input.GetAxisRaw("Mouse Y");
        m_CamY += Input.GetAxisRaw("Mouse X");

        m_CamX = Mathf.Clamp(m_CamX, m_Settings.minClampX, m_Settings.maxClampX);
        Vector3 eulerRot = new Vector3(-m_CamX * m_Settings.vertical, m_CamY * m_Settings.horizontal, 0f);

        m_Settings.crouchingCam.transform.rotation = Quaternion.Euler(eulerRot);
        m_Settings.standingCam.transform.rotation = Quaternion.Euler(eulerRot);
    }

    private void CursorLockState()
    {
        if (m_CursorLocked)
            Cursor.lockState = CursorLockMode.Locked;

        else
            Cursor.lockState = CursorLockMode.None;

    }

    public bool IsCursorLocked()
    {
        return m_CursorLocked;
    }

    public void LockUnlockCursor()
    {
        m_CursorLocked = !m_CursorLocked;

        if (m_CursorLocked) // Locks cursor
        {
            Cursor.visible = false;
        }
        else // Unlocks cursor
        {
            Cursor.visible = true;
        }
    }

    public void DeactivateCrosshair(MonoBehaviour script)
    {
        if (m_crosshairOverride)
			return;
		if (!m_crosshairTaken)
        {
			m_lastOverrider = script;
			m_crosshairTaken = true;
			m_crosshairReleased = false;
		}
	}

    public void ActivateCrosshair(MonoBehaviour script)
    {
		if (m_crosshairOverride)
			return;
        if (!m_crosshairReleased && m_lastOverrider == null)
        {
			m_crosshairTaken = false;
			m_crosshairReleased = true;
		}
        else if (!m_crosshairReleased && m_lastOverrider != null)
        {
            if (script == m_lastOverrider)
            {
				m_crosshairTaken = false;
				m_crosshairReleased = true;
			}
        }
	}

    void UpdateCrosshair()
    {
		m_inventoryToggle = m_inventoryUIObject.activeInHierarchy;
		m_pauseMenuToggle = (m_pauseMenu != null) ? m_pauseMenu.interactable : false;
		m_crosshairOverride = m_inventoryToggle || m_pauseMenuToggle;
		if ((m_crosshairTaken || m_crosshairOverride) && m_crosshair != null)
			m_crosshair.SetActive(false);
        else if (m_crosshair != null)
			m_crosshair.SetActive(true);
	}
}
