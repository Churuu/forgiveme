﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;


[CommandInfo("Music", "Start book music", "Starts the book music")]

public class PlayBookMusicFungus : Command
{
    public override void OnEnter()
    {
        MusicHandler.PlayMusic(MusicHandler.Instance.m_RitualBookSong);
        Continue();
    }
}
