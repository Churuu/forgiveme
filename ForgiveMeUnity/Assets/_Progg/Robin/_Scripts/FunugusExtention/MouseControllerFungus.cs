﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;


[CommandInfo("MouseController", "Lock/Unlock mouse", "Toggles the mouse active/deactive")]
public class MouseControllerFungus : Command
{
    public override void OnEnter()
    {
        MouseController.m_MouseInstance.LockUnlockCursor();
        Continue();
    }
}
