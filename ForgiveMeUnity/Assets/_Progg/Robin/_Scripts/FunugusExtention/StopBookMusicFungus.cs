﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("Music", "Stop book music", "Stops the book music")]

public class StopBookMusicFungus : Command
{
    public override void OnEnter()
    {
        MusicHandler.StopMusic(MusicHandler.Instance.m_RitualBookSong);
        Continue();
    }
}
