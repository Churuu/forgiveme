﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonHighlighter : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    Image m_FadingImage;


    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        LeanTween.alpha(m_FadingImage.rectTransform, 1, 0.5f);
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        LeanTween.alpha(m_FadingImage.rectTransform, 0, 0.5f);
    }

}
