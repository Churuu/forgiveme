﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuHandler : MonoBehaviour
{
    [SerializeField]
    Image m_FadeImage;

    [SerializeField]
    GameObject m_MainMenu, m_WarningMenu, m_Options, m_MainMenuObjects, m_PauseMenu;

    GameObject m_CurrentMenu;

    bool m_IsPaused = false, m_PauseActive = false;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        m_CurrentMenu = m_MainMenu;
        LeanTween.alpha(m_FadeImage.rectTransform, 0, 1);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && m_PauseActive)
            TogglePauseMenu();
    }

    #region Main Menu

    public void LoadScene(string name)
    {
        m_CurrentMenu.GetComponent<CanvasGroup>().interactable = false;
        LeanTween.alpha(m_FadeImage.rectTransform, 1, 1).setOnComplete(() => FadeInScene(name));
    }

    void FadeInScene(string name)
    {
        SceneManager.LoadScene(name);
        LeanTween.alpha(m_FadeImage.rectTransform, 0, 1);
        m_MainMenuObjects.SetActive(false);


        if (name == "GameScene" || name == "GameSceneSave")
            m_PauseActive = true;
        else if (name == "MainMenu")
            DisablePauseMenu();

    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
    }

    public void Options()
    {
        m_CurrentMenu.GetComponent<CanvasGroup>().interactable = false;
        m_CurrentMenu.GetComponent<CanvasGroup>().blocksRaycasts = false;
        LeanTween.alphaCanvas(m_CurrentMenu.GetComponent<CanvasGroup>(), 0, 0.5f).setOnComplete(() => FadeInMenu(m_Options.GetComponent<CanvasGroup>()));
    }

    public void MainMenu()
    {
        if (!m_MainMenuObjects.activeSelf)
            m_MainMenuObjects.SetActive(true);


        m_CurrentMenu.GetComponent<CanvasGroup>().interactable = false;
        m_CurrentMenu.GetComponent<CanvasGroup>().blocksRaycasts = false;
        LeanTween.alphaCanvas(m_CurrentMenu.GetComponent<CanvasGroup>(), 0, 0.5f).setOnComplete(() => FadeInMenu(m_MainMenu.GetComponent<CanvasGroup>()));

    }

    public void WarningMenu()
    {
        m_CurrentMenu.GetComponent<CanvasGroup>().interactable = false;
        m_CurrentMenu.GetComponent<CanvasGroup>().blocksRaycasts = false;
        LeanTween.alphaCanvas(m_CurrentMenu.GetComponent<CanvasGroup>(), 0, 0.5f).setOnComplete(() => FadeInMenu(m_WarningMenu.GetComponent<CanvasGroup>()));
    }

    void FadeInMenu(CanvasGroup canvasGroup)
    {
        LeanTween.alphaCanvas(canvasGroup, 1, 0.5f);
        m_CurrentMenu = canvasGroup.gameObject;
        m_CurrentMenu.GetComponent<CanvasGroup>().interactable = true;
        m_CurrentMenu.GetComponent<CanvasGroup>().blocksRaycasts = true;
    }
    #endregion


    #region Pause Menu
    public void TogglePauseMenu()
    {
        m_IsPaused = !m_IsPaused;
        var canvasGroup = m_PauseMenu.GetComponent<CanvasGroup>();

        if (m_IsPaused)
        {
            canvasGroup.alpha = 1;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
            FindObjectOfType<MouseController>().LockUnlockCursor();
        }
        else
        {
            canvasGroup.alpha = 0;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
            FindObjectOfType<MouseController>().LockUnlockCursor();
        }

    }

    public void DisablePauseMenu()
    {
        var canvasGroup = m_PauseMenu.GetComponent<CanvasGroup>();
        m_IsPaused = false;
        m_PauseActive = false;
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
    }

    public void SaveGame()
    {
        FindObjectOfType<SaveSystem>().SaveGame();
    }

    public void LoadLastSave()
    {
        FindObjectOfType<SaveSystem>().LoadGame();
    }
    #endregion
}
