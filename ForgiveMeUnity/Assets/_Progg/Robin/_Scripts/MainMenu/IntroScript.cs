﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class IntroScript : MonoBehaviour
{
    Image m_FadeImage;

    VideoPlayer videoPlayer;

    private void Start()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        m_FadeImage = GameObject.Find("FadeImage").GetComponent<Image>();
        Invoke("LoadGameScene", Convert.ToSingle(videoPlayer.clip.length));
    }

    void LoadGameScene()
    {
        FindObjectOfType<MenuHandler>().LoadScene("GameScene");
    }
}
