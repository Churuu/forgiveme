﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio;
using FMOD;

public class Options : MonoBehaviour
{

    float m_MasterVolume = 1.0f;
    Bus m_MasterBus;

    // Start is called before the first frame update
    void Start()
    {
        m_MasterVolume = PlayerPrefs.GetFloat("MasterVolume");
        m_MasterBus = FMODUnity.RuntimeManager.GetBus("MasterBus");
        m_MasterBus.setVolume(m_MasterVolume);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
